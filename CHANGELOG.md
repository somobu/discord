# Change log

## v2.4.0: 2022-12-05

* Multiple errors causing voice disconnection has been investigated and eliminated;
* There are a few more errors to fix;

## v2.3.1: 2022-07-20

* Add `getGuildApplicationCommands`, `bulkOverrideGuildApplicationCommands` and `deleteGuildApplicationCommand` to
    `DiscordHttp` class;
* Expose a few `Ready` fields;
* Fix voice disconnect;
* Fire IOException("Websocket is closing") when disconnect() is called before voice is in connected state

## v2.3.0: 2022-07-17

* Add webhook api methods into DiscordHttp;
* Alter Interaction object;
* Fixes on DiscordHttp api;

## v2.2.6: 2022-07-16

* Voice: handle a few corner cases (like Discord not sending us voice data withing 15s);
* Voice: alter an API a bit;


## v2.2.5: 2022-06-19

* Realtime improvements, again;


## v2.2.4: 2022-06-19

* Realtime heart beating improvements;


## v2.2.3: 2022-05-30

* Voice API stability improvements;
* HTTP API Settings refactor;
* Add fields and objects from user-only READY object


## v2.0.2 - v2.2.0: 2021-12-01

* Various changes in Voice API;
* v6 http/ws API removal;
* org.json library was completely replaced with gson


## v2.0.1: 2021-09-05

* Add a small subset of ApplicationCommand mechanism's routes and objects


## v2.0.0: 2021-07-30

* Move current old (v6) API implementation to v6 package;
* Introduce v8 API implementation (com.iillyyaa2033.discord.v8 package);


## v1.10.3: 2020-08-15

* Add 'Bulk Delete Messages' method


## v1.10.2: 2020-07-26

* Introduce `StatusApi` of statuspage.io (endpoint `status.discordapp.com/api/v2`)
* Make `Overwrite` object's fields public


## v1.10.1: 2020-06-16

* Hotfix `DiscordApiVoice`: call method on `IPlaybackListener` after destroying encoder
* Add a few methods to `DiscordApi`


## v1.10.0: 2020-06-08

* Implement transmitting part of VoiceApi
* Add new API: `DiscordApiVoiceBase` for direct access and user-friendly `DiscordApiVoice`


## v1.9.4: 2020-05-22

* Fix `deleteChatMessage` method for bots.


## v1.9.3: 2020-05-22

* Fix bug when one cannot leave any guild (tldr: I should not send `Content-Type` on `DELETE` method).
* Add simple token-only constructor in `DiscordApi`


## v1.9.2: 2020-05-11

* Add `mute_config` field into `ChannelOverride`.


## v1.9.1: 2020-04-21

* Fix bug with duplicate `/v6/` segment in some `DiscordApi` methods.


## v1.9.0: 2020-04-17

* Introduced voice API via `DiscordApiVoice` class and bunch of related objects.
* Replaced `xsalsa20poly1305` with own revision.


## Before
No log available.
