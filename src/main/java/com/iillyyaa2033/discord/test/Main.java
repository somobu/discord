package com.iillyyaa2033.discord.test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.iillyyaa2033.discord.v8.DiscordGateway;
import com.iillyyaa2033.discord.v8.DiscordHttp;
import com.iillyyaa2033.discord.v8.gateway_objs.Ready;
import com.iillyyaa2033.discord.voicev4.DiscordApiVoice;
import com.iillyyaa2033.discord.voicev4.DiscordApiVoiceBase;
import com.iillyyaa2033.discord.voicev4.IPlaybackListener;
import com.iillyyaa2033.discord.voicev4.IVoiceListener;
import com.iillyyaa2033.discord.voicev4.objects.VoiceSpeaking;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

public class Main {

    private static final Gson gson = new GsonBuilder().create();

    private static void allowMethods(String... methods) {
        try {
            Field methodsField = HttpURLConnection.class.getDeclaredField("methods");

            Field modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);
            modifiersField.setInt(methodsField, methodsField.getModifiers() & ~Modifier.FINAL);

            methodsField.setAccessible(true);

            String[] oldMethods = (String[]) methodsField.get(null);
            Set<String> methodsSet = new LinkedHashSet<>(Arrays.asList(oldMethods));
            methodsSet.addAll(Arrays.asList(methods));
            String[] newMethods = methodsSet.toArray(new String[0]);

            methodsField.set(null/*static field*/, newMethods);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new IllegalStateException(e);
//            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws InterruptedException, IOException, UnsupportedAudioFileException {
        allowMethods("PATCH");

        System.load("/home/somobu/gitlab/mudcrab/res/libs/linux-x86-64/libjnopus.so");

        final DiscordHttp api = new DiscordHttp(args[0]);
        final DiscordGateway gateway = new DiscordGateway(args[0]);
        gateway.DEBUG_ALL_INBOX = true;
        gateway.DEBUG_HEARTBEAT = true;
        gateway.setStateListener(new DiscordGateway.StateListener() {

            private static final String TAG = "Gateway/UsrSt";

            @Override
            public void onConnected() {
                log(TAG, "Connected");
            }

            @Override
            public void onReconnected() {
                log(TAG,"Reconnected");
            }

            @Override
            public void onError(String reason) {
                log(TAG,"Error: " + reason);
            }

            @Override
            public void onStopped() {
                log(TAG,"Stopped");
            }
        });
        gateway.setDiscordListener(new DiscordGateway.EventsListener() {
            @Override
            public void onReady(Ready ready) {
                super.onReady(ready);
            }
        });

        gateway.start();
        Thread.sleep(2 * 1000);

        final DiscordApiVoice vc = new DiscordApiVoice(new IVoiceListener() {

            private static final String TAG = "Voice/UserEv";

            @Override
            public void onVoiceReady() {
                log(TAG, "Voice is ready");
            }

            @Override
            public void onError(Exception ex) {
                log(TAG, "Voice error");
                ex.printStackTrace();
            }

            @Override
            public void onClose(boolean remote, int code, String reason) {
                log(TAG, "Closed: " + remote + " " + code + " " + reason);
            }

            @Override
            public void onSpeaking(VoiceSpeaking speaking) {

            }

            @Override
            public void onClientDisconnect(String user_id) {

            }
        });
        DiscordApiVoiceBase.DEBUG_EVENTS = true;
        DiscordApiVoiceBase.DEBUG_HEARTBEAT = true;
        vc.connect(gateway, "489690316690161676", "732679363421536327");


        /*
        mkfifo awo.wav
        {
        youtube-dl -f bestaudio -o - "https://www.youtube.com/watch?v=deCpB1R6fLM" | ffmpeg -y -i pipe: -vn -ar 48000 -ac 2 -f wav "awo.wav" ;
        } || { echo -n > "awo.mp3" ; rm "awo.mp3" ; }
         */
        FileInputStream fis = new FileInputStream("/home/somobu/awo.wav");
        InputStream bufferedIn = new BufferedInputStream(fis);
        AudioInputStream audioStream = AudioSystem.getAudioInputStream(bufferedIn);

        vc.startPlayback(audioStream, new IPlaybackListener() {
            private static final String TAG = "Voice/PlaybackEv";

            @Override
            public void onError(Exception e) {
                log(TAG, "Playback error");
                e.printStackTrace();
            }

            @Override
            public void onFinished() {
                log(TAG, "Playback finished");
            }
        });

        Thread.sleep(Integer.MAX_VALUE);
        vc.disconnect(gateway);
        gateway.stop();

    }

    private static final SimpleDateFormat logSdf = new SimpleDateFormat("dd.MM HH:mm:ss.SSS");

    private static void log(String src, String message) {
        System.out.println(logSdf.format(new Date()) + " [" + src + "] " + message);
    }
}
