package com.iillyyaa2033.discord.statusapi.objects;

public class IncidentComponent {

     public String name;

    public String code;
    public String old_status;
    public String new_status;

    public String id;
    public String status;
    public String created_at;
    public String updated_at;
    public int position;
    public String description;
    public String page_id;

}
