package com.iillyyaa2033.discord.voicev4;

import com.codahale.xsalsa20poly1305.SecretBox;
import com.google.gson.*;
import com.iillyyaa2033.discord.voicev4.objects.VoiceReady;
import com.iillyyaa2033.discord.voicev4.objects.VoiceSessionDescription;
import com.iillyyaa2033.discord.voicev4.objects.VoiceSpeaking;
import com.iillyyaa2033.utils.NetworkUtils;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.io.IOException;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Base, bare-bones Discord Voice Api implementation
 */
public class DiscordApiVoiceBase {

    private static final int API_VER = 4;

    private static final String IDENTIFY_JSON = "{\"op\":0,\"d\":{\"server_id\":\"%s\",\"user_id\":\"%s\",\"session_id\":\"%s\",\"token\":\"%s\"}}";
    private static final String RESUME_JSON = "{\"op\": 7,\"d\": {\"server_id\": \"%s\",\"session_id\": \"%s\",\"token\": \"%s\"}}";
    private static final String SELECT_PROTOCOL_JSON = "{\"op\":1,\"d\":{\"protocol\":\"udp\",\"data\":{\"address\":\"%s\",\"port\":%s,\"mode\":\"%s\"}}}";
    private static final String HEARTBEAT_JSON = "{\"op\":3,\"d\":%s}";
    private static final String SPEAKING_JSON = "{\"op\":5,\"d\":{\"speaking\":%s,\"delay\":0,\"ssrc\":%s}}";

    private static final int MIN_UDP_PORT = 8128;

    private static final String TAG = "VoiceBase";

    public static boolean DEBUG_HEARTBEAT = false;
    public static boolean DEBUG_EVENTS = false;

    private final IVoiceListener listener;
    private boolean isInitialized = false;

    private VoiceReady ready = null;
    private volatile VoiceSessionDescription sessionDesc = null;
    private SecretBox box = null;

    private WebSocketClient client;
    private final DatagramSocket socket;
    private final DatagramPacket inputPacket = new DatagramPacket(new byte[2048], 2048);
    private final DatagramPacket outputPacket = new DatagramPacket(new byte[2048], 2048);

    private char sendSequenceNumber = 0;
    private long sendTimestamp = 4221;

    private byte[] lastFrameHeader = null;
    private final RtpHeader lastParsedHeader = new RtpHeader();

    /**
     * Required to decode json data from Discord
     */
    private final Gson gson = new GsonBuilder().create();

    public DiscordApiVoiceBase(final IVoiceListener listener) throws SocketException {
        int port = MIN_UDP_PORT;
        while (!NetworkUtils.isPortAvailable(port)) port++;

        socket = new DatagramSocket(port);
        socket.setSoTimeout(1000);
        socket.setTrafficClass(0x10);
        this.listener = listener;
    }

    private IOException connectError = null;

    /**
     * Blocking method: will wait till successful voice connection or error
     */
    public void connect(String endpoint, final String serverId, final String userId, final String session,
                        final String token, final boolean resume) throws IOException {
        final String url = "wss://" + endpoint.replace(":80", "") + "/?v=" + API_VER;

        final VoiceHeartbeatThread heartbeatThread = new VoiceHeartbeatThread();

        final Object readyLock = new Object();

        client = new WebSocketClient(URI.create(url)) {

            @Override
            public void onOpen(ServerHandshake handshakedata) {
                if(resume) {
                    send(String.format(RESUME_JSON, serverId, session, token));
                } else {
                    send(String.format(IDENTIFY_JSON, serverId, userId, session, token));
                }
            }

            @Override
            public void onMessage(String message) {
                log(DEBUG_EVENTS, TAG, "Evt: " + message);

                JsonObject full = JsonParser.parseString(message).getAsJsonObject();
                JsonElement data = full.get("d");

                switch (full.getAsJsonPrimitive("op").getAsInt()) {
                    case 2: // OP ready
                        ready = gson.fromJson(data, VoiceReady.class);

                        try {
                            String[] discovered = performIpDiscovery();
                            client.send(String.format(SELECT_PROTOCOL_JSON, discovered[1], discovered[2], "xsalsa20_poly1305"));
                            // And now we'll receive OP4 session description
                        } catch (IOException e) {
                            this.onError(new Exception("Ip discovery failed", e));
                            this.close();
                        }
                        break;
                    case 4: // OP session description
                        sessionDesc = gson.fromJson(data, VoiceSessionDescription.class);
                        box = new SecretBox(sessionDesc.secret_key);

                        // Everything seems fine. Voice ready
                        isInitialized = true;
                        if (listener != null) listener.onVoiceReady();

                        synchronized (readyLock) {
                            readyLock.notifyAll();
                        }
                        break;
                    case 5: // OP speaking
                        if (listener != null) listener.onSpeaking(gson.fromJson(data, VoiceSpeaking.class));
                        break;
                    case 6:
                        // OP ack from discord -- i.e. DC says that they got heartbeat
                        // TODO: here we should mark our connection as 'still-alive'
                        break;
                    case 8: // OP hello
                        heartbeatThread.heartbeat(this, data.getAsJsonObject().getAsJsonPrimitive("heartbeat_interval").getAsInt());
                        break;
                    case 9: // OP resumed
                        // TODO: resume me
                        break;
                    case 13: // OP client disconnect
                        if (listener != null)
                            listener.onClientDisconnect(data.getAsJsonObject().getAsJsonPrimitive("user_id").getAsString());
                        break;
                    default:
                        System.out.println("VWS: onMessage " + message);
                }
            }

            @Override
            public void onClose(int code, String reason, boolean remote) {
                heartbeatThread.interrupt();

                if (listener != null) listener.onClose(remote, code, reason);

                synchronized (readyLock) {
                    connectError = new IOException("Websocket is closing");
                    readyLock.notifyAll();
                }
            }

            @Override
            public void onError(Exception ex) {
                if (listener != null) listener.onError(ex);

                synchronized (readyLock) {
                    connectError = new IOException(ex);
                    readyLock.notifyAll();
                }
            }
        };

        client.connect();

        synchronized (readyLock) {
            long millis = 45 * 1000;

            try {
                readyLock.wait(millis);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (connectError != null) {
                throw connectError;
            }

            if (sessionDesc == null) {
                throw new IOException("We're unable to get Session description from Discord under " + millis + " ms");
            }
        }
    }

    public void disconnect() {
        socket.close();

        try {
            client.closeBlocking();
        } catch (InterruptedException ignored) {

        }

        isInitialized = false;
    }

    public void sendSpeaking(int mask) {
        if (!isInitialized) throw new RuntimeException("Voice is not ready yet");

        client.send(String.format(SPEAKING_JSON, "" + mask, "" + ready.ssrc));
    }

    /**
     * Note: you probably have to sendSpeaking before calling this method
     *
     * <p>
     * As for timestamp increment, see https://lmtools.com/content/rtp-timestamp-calculation
     * </p>
     *
     * @param timestampIncrement how much RTP timestamp should be incremented by this frame
     * @param data               opus-encoded data
     * @throws IOException when something went wrong
     */
    public void sendOpus(long timestampIncrement, byte[] data) throws IOException {
        if (!isInitialized) throw new RuntimeException("Voice is not ready yet");

        if (sendSequenceNumber + 1 > Character.MAX_VALUE - 2) sendSequenceNumber = 0;
        else sendSequenceNumber++;

        sendTimestamp += timestampIncrement;

        byte[] nonce = new byte[24];
        nonce[0] = (byte) (0x80 & 0xFF); // Unsigned version + flags
        nonce[1] = (byte) (0x78 & 0xFF); // Unsigned payload
        nonce[2] = (byte) (sendSequenceNumber >> 8);
        nonce[3] = (byte) (sendSequenceNumber);
        nonce[4] = (byte) (sendTimestamp >> 24);
        nonce[5] = (byte) (sendTimestamp >> 16);
        nonce[6] = (byte) (sendTimestamp >> 8);
        nonce[7] = (byte) (sendTimestamp);

        long s = ready.ssrc;
        nonce[8] = (byte) (s >> 24);
        nonce[9] = (byte) (s >> 16);
        nonce[10] = (byte) (s >> 8);
        nonce[11] = (byte) (s);

        byte[] ciphertext = box.seal(nonce, data);

        byte[] rz = new byte[12 + ciphertext.length];
        System.arraycopy(nonce, 0, rz, 0, 12);
        System.arraycopy(ciphertext, 0, rz, 12, ciphertext.length);

        outputPacket.setData(rz);
        outputPacket.setLength(rz.length);
        outputPacket.setAddress(InetAddress.getByName(ready.ip));
        outputPacket.setPort(ready.port);

        socket.send(outputPacket);
    }

    /**
     * Receives next UDP packet
     *
     * @return opus-encoded voice data or null if none
     * @throws IOException when something went wrong
     */
    public byte[] receiveOpus() throws IOException {
        if (!isInitialized) throw new RuntimeException("Voice is not ready yet");

        socket.receive(inputPacket);

        lastFrameHeader = new byte[24];
        System.arraycopy(inputPacket.getData(), 0, lastFrameHeader, 0, 12);

        byte[] encrypted = new byte[inputPacket.getLength() - 12];
        System.arraycopy(inputPacket.getData(), 12, encrypted, 0, inputPacket.getLength() - 12);

        byte[] opt = box.open(lastFrameHeader, encrypted);

        if (opt != null) {
            int offset = calculateRtpOffset(opt);

            byte[] data = new byte[opt.length - offset];
            System.arraycopy(opt, offset, data, 0, data.length);

            return data;
        }

        return null;
    }

    public RtpHeader parseLastHeader() {
        if (lastFrameHeader == null) throw new RuntimeException("There are no successfully received packets");

        // Reset values
        lastParsedHeader.version = 0;
        lastParsedHeader.sequence_number = 0;
        lastParsedHeader.timestamp = 0;
        lastParsedHeader.ssrc = 0;

        lastParsedHeader.version += lastFrameHeader[0]; // Version+flags
        // Payload omitted
        lastParsedHeader.sequence_number += lastFrameHeader[2] << 8;
        lastParsedHeader.sequence_number += lastFrameHeader[3];
        lastParsedHeader.timestamp += lastFrameHeader[4] << 24;
        lastParsedHeader.timestamp += lastFrameHeader[5] << 16;
        lastParsedHeader.timestamp += lastFrameHeader[6] << 8;
        lastParsedHeader.timestamp += lastFrameHeader[7];

        lastParsedHeader.ssrc += lastFrameHeader[8] << 24;
        lastParsedHeader.ssrc += lastFrameHeader[9] << 16;
        lastParsedHeader.ssrc += lastFrameHeader[10] << 8;
        lastParsedHeader.ssrc += lastFrameHeader[11];

        return lastParsedHeader;
    }

    private String[] performIpDiscovery() throws IOException {

        byte[] data = new byte[70];
        DatagramPacket packet = new DatagramPacket(data, data.length, InetAddress.getByName(ready.ip), ready.port);
        socket.send(packet);

        data = new byte[128];
        packet = new DatagramPacket(data, data.length);
        socket.receive(packet);

        data = packet.getData();

        long ssrc = data[0] * 256 * 256 * 256 + data[1] * 256 * 256 + data[2] * 256 + data[3];

        int terminatorIdx;
        for (terminatorIdx = 4; terminatorIdx < 68 && data[terminatorIdx] != 0; terminatorIdx++) ;
        String address = new String(data, 4, terminatorIdx - 4);

        int port = data[68] * 256 + data[69];

        return new String[]{"" + ssrc, address, "" + port};
    }

    /**
     * RU: дискорд пихает в начало voice data один или несколько RTP extension'ов.
     * <br/>Данный метод позволяет рассчитать, где заканчиваются эти экстеншены и начинается опусовский звук
     *
     * @param data 'voice data' from UDP packet
     * @return index of first byte of opus-encoded voice
     */
    private static int calculateRtpOffset(byte[] data) {
        int offset = 0;

        if (data[0] == -66 && data[1] == -34) { // First bytes is BE DE
            int extCount = data[2] * 256 + data[3];
            int currentOffset = 4;

            // Skip RTP extensions
            for (int currentExt = 0; currentExt < extCount; currentExt++) {
                int extLen = (data[currentOffset] & 0xF) + 2;
                currentOffset += extLen;
            }

            // Skip zero pads
            while (currentOffset < data.length && data[currentOffset] == 0) currentOffset++;

            offset = currentOffset;
        }

        return offset;
    }


    static class RtpHeader {

        public char version;
        public int sequence_number;
        public long timestamp;
        public long ssrc;

    }

    private static class VoiceHeartbeatThread extends Thread {

        private WebSocketClient client;
        private long interval = 41250;
        private long nonce = 0;

        public void heartbeat(WebSocketClient client, long interval) {
            this.client = client;
            this.interval = interval;
            start();
        }

        public long getLastNonce() {
            return nonce;
        }

        @Override
        public void run() {
            log(DEBUG_HEARTBEAT, TAG, "Voice heartbeat started with interval " + interval);

            while (!isInterrupted()) {

                nonce = System.currentTimeMillis();
                if (client.isOpen()) client.send(String.format(HEARTBEAT_JSON, "" + nonce));
                else break;

                log(DEBUG_HEARTBEAT, TAG, "Nonce " + nonce + " sent");

                try {
                    sleep(interval - interval / 5);
                } catch (InterruptedException ignored) {
                    break;
                }

            }

            log(DEBUG_HEARTBEAT, TAG, "Voice heartbeat stopped");
        }
    }

    private static final SimpleDateFormat logSdf = new SimpleDateFormat("dd.MM HH:mm:ss.SSS");

    private static void log(boolean level, String src, String message) {
        if (level) System.out.println(logSdf.format(new Date()) + " [" + src + "] " + message);
    }

}
