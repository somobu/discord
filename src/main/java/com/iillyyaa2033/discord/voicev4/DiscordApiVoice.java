package com.iillyyaa2033.discord.voicev4;

import com.iillyyaa2033.discord.v8.DiscordGateway;
import com.iillyyaa2033.discord.v8.gateway_objs.VoiceServerUpdated;
import com.iillyyaa2033.discord.v8.objects.VoiceState;
import com.iillyyaa2033.discord.voicev4.objects.VoiceSpeaking;
import org.java_websocket.exceptions.WebsocketNotConnectedException;
import org.jitsi.impl.neomedia.codec.audio.opus.Opus;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import java.io.IOException;

public class DiscordApiVoice {

    private static final int FRAME_DURATION = 20; // ms
    private static final int FRAME_SIZE = 4; // bytes
    private static final int CHANNELS = 2;
    private static final int SAMPLE_SIZE = 16; // bits
    private static final int SAMPLE_RATE = 48000; // Hz
    private static final int SAMPLE_COUNT = FRAME_DURATION * (SAMPLE_RATE / 1000);
    private static final int PLAYER_BUFFER = FRAME_SIZE * SAMPLE_COUNT; // bytes

    private DiscordGateway realtime;

    private String guild = null;
    private String channel = null;

    private boolean selfMute = false;
    private boolean selfDeaf = false;

    private boolean connected = false;
    private boolean speaking = false;

    private DiscordApiVoiceBase voice;
    public boolean suppressFinishCallback = false;

    private final MyVoiceListener voiceBaseListener;
    private final RealtimeListener realtimeListener = new RealtimeListener();

    private VoiceReadyLock lock = new VoiceReadyLock();
    private ConvertedPlaybackThread player = null;

    public DiscordApiVoice(IVoiceListener listener) {
        voiceBaseListener = new MyVoiceListener(listener);
    }

    /**
     * Simplified version of connect(...): selfMute and selfDeaf are both set to false
     */
    public synchronized void connect(DiscordGateway realtime, final String guild, String channel) throws IOException {
        connect(realtime, guild, channel, false, false, true);
    }

    /**
     * Blocking method: will wait till successful voice connection or error
     *
     * @param tamperWithRealtime is a workaround param that should be removed on refactor
     */
    public synchronized void connect(final DiscordGateway realtime, final String guild, String channel,
                                     boolean selfMute, boolean selfDeaf, boolean tamperWithRealtime) throws IOException {
        if (connected) throw new RuntimeException("This api is already connected to endpoint");

        this.realtime = realtime;

        this.guild = guild;
        this.channel = channel;

        this.selfMute = selfMute;
        this.selfDeaf = selfDeaf;

        lock.pass();
        lock = new VoiceReadyLock();

        if (tamperWithRealtime) realtime.addVRL(realtimeListener);
        realtime.sendVoiceUptate(guild, channel, selfMute, selfDeaf);

        long sleepMillis = 15 * 1000;
        lock.waitReady(sleepMillis);

        if (lock.endpoint == null || lock.voiceState == null) {
            suppressFinishCallback = false;
            stopPlayback();

            throw new IOException("Unable to get endpoint / voice state from Discord under " + sleepMillis + " ms");
        }

        voice = new DiscordApiVoiceBase(voiceBaseListener);
        voice.connect(lock.endpoint, lock.guild, lock.voiceState.user_id, lock.voiceState.session_id, lock.token, false);

        connected = true;
    }

    public synchronized void disconnect(DiscordGateway realtime) {
        disconnect(realtime, true);
    }

    public synchronized void disconnect(DiscordGateway realtime, boolean tamperWithRealtime) {
        if (realtime != null) {
            try {
                realtime.sendVoiceUptate(guild, null, false, false);
            } catch (WebsocketNotConnectedException ignored) {
                // This exception is not welcome here
            }

            if (tamperWithRealtime) realtime.removeVRL(realtimeListener);

        } else {
            throw new RuntimeException("DiscordApiVoice: disconnect from null realtime!!");
        }

        stopPlayback();
        voice.disconnect();
        connected = false;
    }

    public void startPlayback(AudioInputStream source, IPlaybackListener listener) throws IOException {
        stopPlayback();

        realtimeListener.setWorkaroundData(source, listener);

        player = new ConvertedPlaybackThread(listener, source);
        player.start();
    }

    public void stopPlayback() {

        if (player != null) {
            player.interrupt();
        }

        player = null;
    }

    private void scheduleReconnect(final DiscordGateway realtime) {
        new Thread() {

            @Override
            public void run() {
                try {
                    Thread.sleep(250);
                } catch (InterruptedException ignored) {

                }

                realtimeListener.realtimeGoingToReconnect(realtime);
                realtimeListener.realtimeReady(realtime);
            }
        }.start();
    }

    private static class VoiceReadyLock {

        private volatile String endpoint, guild, token;
        private volatile VoiceState voiceState;

        private final Object endpointLock = new Object();
        private final Object voiceLock = new Object();

        void waitReady(long millis) {

            synchronized (voiceLock) {
                voiceState = null;
            }

            synchronized (endpointLock) {
                try {
                    endpointLock.wait(millis);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (voiceState == null) {
                synchronized (voiceLock) {
                    try {
                        voiceLock.wait(millis);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        void onServer(String endpoint, String guild, String token) {
            synchronized (endpointLock) {
                this.endpoint = endpoint;
                this.guild = guild;
                this.token = token;
                endpointLock.notifyAll();
            }
        }

        void onVoiceState(VoiceState voiceState) {
            synchronized (voiceLock) {
                this.voiceState = voiceState;
                voiceLock.notifyAll();
            }
        }

        void pass() {
            synchronized (voiceLock) {
                voiceLock.notifyAll();
            }
        }
    }

    class ConvertedPlaybackThread extends Thread {

        final IPlaybackListener listener;

        final AudioInputStream stream;

        private long encoderState;

        ConvertedPlaybackThread(IPlaybackListener listener, AudioInputStream source) throws IOException {
            this.listener = listener;

            AudioFormat audioFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, SAMPLE_RATE, SAMPLE_SIZE, CHANNELS, FRAME_SIZE, SAMPLE_RATE, false);
            this.stream = AudioSystem.getAudioInputStream(audioFormat, source);

            prepareEncoder(audioFormat);
        }

        @Override
        public void run() {

            long started = System.currentTimeMillis();
            long packetNum = 0;
            int nBytesRead = 0;
            boolean playbackSuccess = false;

            try {
                // Ensure we're not speaking right now
                if (speaking) {
                    speaking = false;
                    voice.sendSpeaking(0);
                }

                byte[] decodedData = new byte[PLAYER_BUFFER];

                while (nBytesRead != -1 && !isInterrupted()) {

                    nBytesRead = stream.read(decodedData, 0, decodedData.length);
                    if (nBytesRead >= 0) {
                        sendPCM(decodedData);
                        packetNum++;

                        try {
                            long now = System.currentTimeMillis();
                            long rt = now - started; // actual
                            long pt = packetNum * FRAME_DURATION; // planned
                            long diff = rt - pt;
                            if (FRAME_DURATION - diff > 0) {
                                Thread.sleep(FRAME_DURATION - diff);
                            }
                        } catch (InterruptedException ignored) {
                            break;
                        }
                    }

                }

                playbackSuccess = true;
            } catch (IOException e) {
                if (listener != null) listener.onError(e);
            } finally {
                destroyEncoder();
            }

            // We're not speaking anymore
            speaking = false;
            try {
                voice.sendSpeaking(0);
            } catch (Exception ignored) {
                // We may be disconnected now
            }

            if (!suppressFinishCallback && listener != null) {
                listener.onFinished();
            }
        }

        private void prepareEncoder(AudioFormat audioFormat) throws IOException {
            if (audioFormat.getChannels() != CHANNELS)
                throw new IOException("Channels should be exactly " + CHANNELS + "");
            if (audioFormat.getSampleRate() != SAMPLE_RATE)
                throw new IOException("Sample rate should be exactly " + SAMPLE_RATE + " Hz");

            encoderState = Opus.encoder_create(SAMPLE_RATE, CHANNELS);
            if (encoderState <= 0) throw new IOException("Could not create encoder");
        }

        private void destroyEncoder() {
            if (encoderState > 0) {
                Opus.encoder_destroy(encoderState);
                encoderState = -1;
            }
        }

        /**
         * Send raw pcm data.
         *
         * <p>
         * You should call prepareEncoder() first.
         * </p>
         */
        private void sendPCM(byte[] pcmData) throws IOException {
            if (encoderState <= 0) throw new IOException("Encoder is not prepared");

            if (!speaking) {
                speaking = true;
                voice.sendSpeaking(1);
            }

            byte[] encodedData = new byte[pcmData.length];

            int written = Opus.encode(encoderState, pcmData, 0, SAMPLE_COUNT, encodedData, 0, encodedData.length);
            if (written < 0) {
                throw new IOException("Could not encode data");
            } else {
                byte[] data = new byte[written];
                System.arraycopy(encodedData, 0, data, 0, written);
                voice.sendOpus(SAMPLE_COUNT, data);
            }
        }
    }

    class MyVoiceListener implements IVoiceListener {

        private final IVoiceListener clientVoiceListener;

        MyVoiceListener(IVoiceListener clientVoiceListener) {
            this.clientVoiceListener = clientVoiceListener;
        }

        @Override
        public void onVoiceReady() {
            if (clientVoiceListener == null) return;
            if (suppressFinishCallback) return;

            clientVoiceListener.onVoiceReady();
        }

        @Override
        public void onError(Exception ex) {
            if (clientVoiceListener == null) return;
            clientVoiceListener.onError(ex);
        }

        @Override
        public void onClose(boolean remote, int code, String reason) {
            if (clientVoiceListener == null) return;
            if (suppressFinishCallback) return;

            if(code == 1006) {
                System.out.println("Got code 1006, suppose realtime will reconnect us soon");
                realtimeListener.realtimeGoingToReconnect(realtime);
            } else {
                clientVoiceListener.onClose(remote, code, reason);
            }
        }

        @Override
        public void onSpeaking(VoiceSpeaking speaking) {
            if (clientVoiceListener == null) return;
            clientVoiceListener.onSpeaking(speaking);
        }

        @Override
        public void onClientDisconnect(String user_id) {
            if (clientVoiceListener == null) return;
            clientVoiceListener.onClientDisconnect(user_id);
        }
    }

    class RealtimeListener implements DiscordGateway.IVoiceEventsListener {

        private AudioInputStream resumeWorkaroundAIS = null;
        private IPlaybackListener resumeWorkaroundL = null;

        public void setWorkaroundData(AudioInputStream stream, IPlaybackListener listener) {
            this.resumeWorkaroundAIS = stream;
            this.resumeWorkaroundL = listener;
        }

        @Override
        public void onVoiceServerUpdate(DiscordGateway realtime, VoiceServerUpdated vsu) {
            if (vsu.guild_id.equals(guild)) {
                lock.onServer(vsu.endpoint, vsu.guild_id, vsu.token);
            }

            if (connected && vsu.guild_id.equals(guild)) {
                scheduleReconnect(realtime);
            }
        }

        @Override
        public void onVoiceStateUpdate(DiscordGateway realtime, VoiceState voiceState) {
            if (voiceState.guild_id.equals(guild)) {
                lock.onVoiceState(voiceState);
            }
        }

        @Override
        public void realtimeGoingToReconnect(DiscordGateway realtime) {
            suppressFinishCallback = true;

            stopPlayback();
            disconnect(realtime, false);
        }

        @Override
        public void realtimeReady(DiscordGateway realtime) {
            suppressFinishCallback = false;

            try {
                connect(realtime, guild, channel, selfMute, selfDeaf, false);

                if (resumeWorkaroundAIS != null) {
                    startPlayback(resumeWorkaroundAIS, resumeWorkaroundL);
                }

            } catch (IOException e) {
                voiceBaseListener.onError(e);

                disconnect(realtime, false);
            }
        }
    }

}
