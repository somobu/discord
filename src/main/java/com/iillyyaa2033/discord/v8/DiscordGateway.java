package com.iillyyaa2033.discord.v8;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.iillyyaa2033.discord.v8.gateway_objs.*;
import com.iillyyaa2033.discord.v8.objects.*;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.exceptions.WebsocketNotConnectedException;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DiscordGateway {

    private static final String wsUrl = "wss://gateway.discord.gg/?encoding=json&v=8";

    private final Gson gson = new GsonBuilder().create();
    private volatile WSClient ws;
    private final HeartbeatThread hb = new HeartbeatThread();

    private StateListener stateListener = null;
    private EventsListener discordListener = null;
    private final List<IVoiceEventsListener> voiceListeners = Collections.synchronizedList(new ArrayList<IVoiceEventsListener>());

    private final String token;
    private long intents = Identify.ALL_INTENTS;
    private String sessionId = null;

    private volatile long lastSequenceNumber = 0;
    private volatile long lastAckTime = 0;

    private UpdateStatus lastSetPresence = null;

    public boolean DEBUG_ALL_INBOX = false;
    public boolean DEBUG_ALL_EXCEPT_DISPATCH = false;
    public boolean DEBUG_UNKNOWN_EVENTS = true;
    public boolean DEBUG_HEARTBEAT = false;

    public DiscordGateway(String token) {
        ws = new WSClient(URI.create(wsUrl));
        this.token = token;
    }

    public void setStateListener(StateListener listener) {
        this.stateListener = listener;
    }

    public void setDiscordListener(EventsListener listener) {
        this.discordListener = listener;
    }

    public void setIntents(long intents) {
        this.intents = intents;
    }

    public void setInitialPresence(UpdateStatus presence) {
        this.lastSetPresence = presence;
    }

    public void start() {
        ws.connect();
    }

    public void stop() {
        hb.die();
        if (ws != null && !ws.isClosed()) ws.close();
        if (stateListener != null) stateListener.onStopped();
    }

    public void updatePresence(UpdateStatus status) {
        this.lastSetPresence = status;
        ws.send(WSClient.PRESENCE_UPDATE, gson.toJson(status));
    }

    public void sendVoiceUptate(String guild, String channel, boolean selfMute, boolean selfDeaf) {
        if (channel == null) {
            // This is ugly but I don't care now
            ws.send(WSClient.VOICE_STATE_UPDATE, String.format("{\"guild_id\":\"%s\",\"channel_id\":null}", guild));
        } else {
            VoiceState vs = new VoiceState();
            vs.guild_id = guild;
            vs.channel_id = channel;
            vs.self_mute = selfMute;
            vs.self_deaf = selfDeaf;
            ws.send(WSClient.VOICE_STATE_UPDATE, gson.toJson(vs));
        }
    }

    public interface StateListener {

        void onConnected();

        void onReconnected();

        void onError(String reason);

        void onStopped();

    }

    public interface IVoiceEventsListener {

        void onVoiceServerUpdate(DiscordGateway realtime, VoiceServerUpdated voiceServer);

        void onVoiceStateUpdate(DiscordGateway realtime, VoiceState voiceState);

        void realtimeGoingToReconnect(DiscordGateway realtime);

        void realtimeReady(DiscordGateway realtime);
    }

    public void addVRL(IVoiceEventsListener l) {
        synchronized (voiceListeners) {
            log(true, "GW/VRL", "Adding voice recon listener");
            voiceListeners.add(l);
        }
    }

    public void removeVRL(IVoiceEventsListener l) {
        synchronized (voiceListeners) {
            log(true, "GW/VRL", "Removing voice recon listener");
            voiceListeners.remove(l);
        }
    }

    public static abstract class EventsListener {

        public void onReady(Ready ready) {
        }

        public void onResumed() {
        }

        // Channels

        public void onChannelCreate(Channel channel) {
        }

        public void onChannelUpdate(Channel channel) {
        }

        public void onChannelDelete(Channel channel) {
        }

        public void onChannelPinsUpdate(ChannelPinsUpdate channel) {
        }

        // Guild

        public void onGuildCreate(Guild guild) {
        }

        public void onGuildUpdate(Guild guild) {
        }

        public void onGuildDelete(GuildUnavailable guild) {
        }

        public void onGuildBanAdd(GuildBanEvent event) {
        }

        public void onGuildBanRemove(GuildBanEvent event) {
        }

        public void onGuildEmojisUpdate(EmojisUpdated event) {
        }

        public void onGuildIntegrationsUpdate(IntegrationsUpdated event) {
        }

        public void onGuildMemberAdd(GuildMember member) {
        }

        public void onGuildMemberRemove(GuildMemberRemoved member) {
        }

        public void onGuildMemberUpdate(GuildMemberUpdated member) {
        }

        public void onGuildMembersChunk(GuildMembersChunk chunk) {
        }

        public void onGuildRoleCreate(RoleEvent event) {
        }

        public void onGuildRoleUpdate(RoleEvent event) {
        }

        public void onGuildRoleDelete(RoleEvent event) {
        }

        // Invite

        public void onInviteCreate(InviteCreated event) {
        }

        public void onInviteDelete(InviteDeleted event) {
        }

        // Messages

        public void onMessageCreate(Message message) {
        }

        public void onMessageUpdate(Message message) {
        }

        public void onMessageDelete(MessageDeleted message) {
        }

        public void onMessageDeleteBulk(MessageBulkDeleted message) {
        }

        public void onMessageReactionAdd(MessageReactionEvent event) {
        }

        public void onMessageReactionRemove(MessageReactionEvent event) {
        }

        public void onMessageReactionRemoveAll(MessageReactionEvent event) {
        }

        public void onMessageReactionRemoveEmoji(MessageReactionEvent event) {
        }

        // Presence

        public void onPresenceUpdate(PresenceUpdate event) {
        }

        public void onTypingStart(TypingStart start) {
        }

        public void onUserUpdate(User user) {
        }

        // Voice

        public void onVoiceStateUpdate(VoiceState event) {
        }

        public void onVoiceServerUpdate(VoiceServerUpdated event) {
        }

        // Webhooks

        public void onWebhookUpdate(WebhookUpdated updated) {
        }

        // Commands

        public void onCommandCreate(SlashCommand command) {
        }

        public void onCommandUpdate(SlashCommand command) {
        }

        public void onCommandDelete(SlashCommand command) {
        }

        // Interactions

        public void onInteractionCreate(Interaction event) {
        }

    }

    private Identify buildIdentify() {
        Identify identify = new Identify();
        identify.token = token;
        identify.intents = intents;
        identify.presence = lastSetPresence;
        return identify;
    }

    private Resume buildResume() {
        Resume resume = new Resume();
        resume.token = token;
        resume.session_id = sessionId;
        resume.seq = lastSequenceNumber;
        return resume;
    }

    private synchronized void dropSessionData() {
        this.sessionId = null;
        this.lastSequenceNumber = 0;
        this.lastAckTime = 0;
    }

    private class WSClient extends WebSocketClient {

        private static final String message = "{\"op\":%d,\"d\":%s}";

        private static final String TAG_RX = "Gateway/RX";
        private static final String TAG_TX = "Gateway/TX";

        private static final int DISPATCH = 0;
        private static final int HEARTBEAT = 1;
        private static final int IDENTIFY = 2;
        static final int PRESENCE_UPDATE = 3;
        static final int VOICE_STATE_UPDATE = 4;
        private static final int UNKNOWN_5 = 5;
        private static final int RESUME = 6;
        private static final int RECONNECT = 7;
        private static final int REQUEST_GUILD_MEMBERS = 8;
        private static final int INVALID_SESSION = 9;
        private static final int HELLO = 10;
        private static final int HEARTBEAT_ACK = 11;

        private final ExecutorService callbacksExecutor = Executors.newCachedThreadPool();

        public WSClient(URI serverUri) {
            super(serverUri);
        }

        public void send(int op, String d) {
            log(DEBUG_ALL_INBOX, TAG_TX, op + " " + d);
            send(String.format(message, op, d));
        }

        @Override
        public void onOpen(ServerHandshake handshakedata) {

        }

        @Override
        public void onMessage(final String message) {
            callbacksExecutor.submit(new Runnable() {
                @Override
                public void run() {
                    GatewayPayload payload = gson.fromJson(message, GatewayPayload.class);

                    log(DEBUG_ALL_INBOX, TAG_RX, message);
                    log(!DEBUG_ALL_INBOX && DEBUG_ALL_EXCEPT_DISPATCH && payload.op != DISPATCH, TAG_RX, message);

                    onMessage(payload);
                }
            });

        }

        private void onMessage(GatewayPayload message) {

            if (message.s > 0 && message.s > lastSequenceNumber)
                lastSequenceNumber = message.s;

            switch (message.op) {
                case HELLO:
                    // First, start heartbeating
                    hb.setNewHeartbeat(message.d.getAsJsonObject().get("heartbeat_interval").getAsInt());

                    if (sessionId == null) ws.send(IDENTIFY, gson.toJson(buildIdentify()));
                    else ws.send(RESUME, gson.toJson(buildResume()));
                    break;
                case DISPATCH:
                    onDispatch(message.t, message.d);
                    break;
                case RECONNECT:
                    foreachListener(new IForEachVoiceListener() {
                        @Override
                        public void each(IVoiceEventsListener l) {
                            l.realtimeGoingToReconnect(DiscordGateway.this);
                        }
                    });
                    dropSessionData();
                    hb.reconnect();
                    break;
                case INVALID_SESSION:
                    if (!message.d.getAsBoolean()) dropSessionData();
                    hb.reconnect();
                    break;
                case HEARTBEAT_ACK:
                    lastAckTime = System.currentTimeMillis();
                    break;
                default:
                    log(DEBUG_UNKNOWN_EVENTS, TAG_RX, "Unknown: OP:" + message.op + " | sq:" + message.s +
                            " | t:" + message.t + "\n\td:" + message.d);

            }
        }

        private void onDispatch(String type, JsonElement data) {
            switch (type) {
                case "READY":
                    Ready ready = gson.fromJson(data, Ready.class);
                    sessionId = ready.session_id;

                    foreachListener(new IForEachVoiceListener() {
                        @Override
                        public void each(IVoiceEventsListener l) {
                            l.realtimeReady(DiscordGateway.this);
                        }
                    });

                    if (discordListener != null) discordListener.onReady(ready);
                    if (stateListener != null) stateListener.onConnected();
                    break;
                case "RESUMED":
                    foreachListener(new IForEachVoiceListener() {
                        @Override
                        public void each(IVoiceEventsListener l) {
                            l.realtimeReady(DiscordGateway.this);
                        }
                    });

                    if (discordListener != null) discordListener.onResumed();
                    break;

                // TODO: replace all code below with reflections on interface

                case "CHANNEL_CREATE":
                    if (discordListener != null) discordListener.onChannelCreate(gson.fromJson(data, Channel.class));
                    break;
                case "CHANNEL_UPDATE":
                    if (discordListener != null) discordListener.onChannelUpdate(gson.fromJson(data, Channel.class));
                    break;
                case "CHANNEL_DELETE":
                    if (discordListener != null) discordListener.onChannelDelete(gson.fromJson(data, Channel.class));
                    break;
                case "CHANNEL_PINS_UPDATE":
                    if (discordListener != null)
                        discordListener.onChannelPinsUpdate(gson.fromJson(data, ChannelPinsUpdate.class));
                    break;

                case "GUILD_CREATE":
                    if (discordListener != null) discordListener.onGuildCreate(gson.fromJson(data, Guild.class));
                    break;
                case "GUILD_UPDATE":
                    if (discordListener != null) discordListener.onGuildUpdate(gson.fromJson(data, Guild.class));
                    break;
                case "GUILD_DELETE":
                    if (discordListener != null)
                        discordListener.onGuildDelete(gson.fromJson(data, GuildUnavailable.class));
                    break;
                case "GUILD_BAN_ADD":
                    if (discordListener != null)
                        discordListener.onGuildBanAdd(gson.fromJson(data, GuildBanEvent.class));
                    break;
                case "GUILD_BAN_REMOVE":
                    if (discordListener != null)
                        discordListener.onGuildBanRemove(gson.fromJson(data, GuildBanEvent.class));
                    break;
                case "GUILD_EMOJIS_UPDATE":
                    if (discordListener != null)
                        discordListener.onGuildEmojisUpdate(gson.fromJson(data, EmojisUpdated.class));
                    break;
                case "GUILD_INTEGRATIONS_UPDATE":
                    if (discordListener != null)
                        discordListener.onGuildIntegrationsUpdate(gson.fromJson(data, IntegrationsUpdated.class));
                    break;
                case "GUILD_MEMBER_ADD":
                    if (discordListener != null)
                        discordListener.onGuildMemberAdd(gson.fromJson(data, GuildMember.class));
                    break;
                case "GUILD_MEMBER_REMOVE":
                    if (discordListener != null)
                        discordListener.onGuildMemberRemove(gson.fromJson(data, GuildMemberRemoved.class));
                    break;
                case "GUILD_MEMBER_UPDATE":
                    if (discordListener != null)
                        discordListener.onGuildMemberUpdate(gson.fromJson(data, GuildMemberUpdated.class));
                    break;
                case "GUILD_MEMBERS_CHUNK":
                    if (discordListener != null)
                        discordListener.onGuildMembersChunk(gson.fromJson(data, GuildMembersChunk.class));
                    break;
                case "GUILD_ROLE_CREATE":
                    if (discordListener != null)
                        discordListener.onGuildRoleCreate(gson.fromJson(data, RoleEvent.class));
                    break;
                case "GUILD_ROLE_UPDATE":
                    if (discordListener != null)
                        discordListener.onGuildRoleUpdate(gson.fromJson(data, RoleEvent.class));
                    break;
                case "GUILD_ROLE_DELETE":
                    if (discordListener != null)
                        discordListener.onGuildRoleDelete(gson.fromJson(data, RoleEvent.class));
                    break;

                case "INVITE_CREATE":
                    if (discordListener != null)
                        discordListener.onInviteCreate(gson.fromJson(data, InviteCreated.class));
                    break;
                case "INVITE_DELETE":
                    if (discordListener != null)
                        discordListener.onInviteDelete(gson.fromJson(data, InviteDeleted.class));
                    break;

                case "MESSAGE_CREATE":
                    if (discordListener != null) discordListener.onMessageCreate(gson.fromJson(data, Message.class));
                    break;
                case "MESSAGE_UPDATE":
                    if (discordListener != null) discordListener.onMessageUpdate(gson.fromJson(data, Message.class));
                    break;
                case "MESSAGE_DELETE":
                    if (discordListener != null)
                        discordListener.onMessageDelete(gson.fromJson(data, MessageDeleted.class));
                    break;
                case "MESSAGE_DELETE_BULK":
                    if (discordListener != null)
                        discordListener.onMessageDeleteBulk(gson.fromJson(data, MessageBulkDeleted.class));
                    break;
                case "MESSAGE_REACTION_ADD":
                    if (discordListener != null)
                        discordListener.onMessageReactionAdd(gson.fromJson(data, MessageReactionEvent.class));
                    break;
                case "MESSAGE_REACTION_REMOVE":
                    if (discordListener != null)
                        discordListener.onMessageReactionRemove(gson.fromJson(data, MessageReactionEvent.class));
                    break;
                case "MESSAGE_REACTION_REMOVE_ALL":
                    if (discordListener != null)
                        discordListener.onMessageReactionRemoveAll(gson.fromJson(data, MessageReactionEvent.class));
                    break;
                case "MESSAGE_REACTION_REMOVE_EMOJI":
                    if (discordListener != null)
                        discordListener.onMessageReactionRemoveEmoji(gson.fromJson(data, MessageReactionEvent.class));
                    break;

                case "PRESENCE_UPDATE":
                    if (discordListener != null)
                        discordListener.onPresenceUpdate(gson.fromJson(data, PresenceUpdate.class));
                    break;
                case "TYPING_START":
                    if (discordListener != null) discordListener.onTypingStart(gson.fromJson(data, TypingStart.class));
                    break;
                case "USER_UPDATE":
                    if (discordListener != null) discordListener.onUserUpdate(gson.fromJson(data, User.class));
                    break;

                case "VOICE_STATE_UPDATE":
                    final VoiceState vsc = gson.fromJson(data, VoiceState.class);

                    foreachListener(new IForEachVoiceListener() {
                        @Override
                        public void each(IVoiceEventsListener l) {
                            l.onVoiceStateUpdate(DiscordGateway.this, vsc);
                        }
                    });

                    if (discordListener != null) {
                        discordListener.onVoiceStateUpdate(vsc);
                    }

                    break;
                case "VOICE_SERVER_UPDATE":
                    final VoiceServerUpdated vsu = gson.fromJson(data, VoiceServerUpdated.class);

                    foreachListener(new IForEachVoiceListener() {
                        @Override
                        public void each(IVoiceEventsListener l) {
                            l.onVoiceServerUpdate(DiscordGateway.this, vsu);
                        }
                    });

                    if (discordListener != null) {
                        discordListener.onVoiceServerUpdate(vsu);
                    }

                    break;

                case "WEBHOOKS_UPDATE":
                    if (discordListener != null)
                        discordListener.onWebhookUpdate(gson.fromJson(data, WebhookUpdated.class));
                    break;

                case "APPLICATION_COMMAND_CREATE":
                    if (discordListener != null)
                        discordListener.onCommandCreate(gson.fromJson(data, SlashCommand.class));
                    break;
                case "APPLICATION_COMMAND_UPDATE":
                    if (discordListener != null)
                        discordListener.onCommandUpdate(gson.fromJson(data, SlashCommand.class));
                    break;
                case "APPLICATION_COMMAND_DELETE":
                    if (discordListener != null)
                        discordListener.onCommandDelete(gson.fromJson(data, SlashCommand.class));
                    break;

                case "INTERACTION_CREATE":
                    if (discordListener != null)
                        discordListener.onInteractionCreate(gson.fromJson(data, Interaction.class));
                    break;
                default:
                    log(DEBUG_UNKNOWN_EVENTS, TAG_RX, "Unknown dispatch: " + type + " d: " + data);
            }
        }

        @Override
        public void onClose(int code, String reason, boolean remote) {
            log(DEBUG_UNKNOWN_EVENTS, TAG_RX, "onClose: " + code + " " + reason);

            // Just ignore this -- if socket closed by ours it is fine.
            // If error occurred, heartbeat thread will notice it anyway.

            // Error code 1006 is really strange
            if(code == 1006){
                // In this case, we'll notify our voice listeners
                foreachListener(new IForEachVoiceListener() {
                    @Override
                    public void each(IVoiceEventsListener l) {
                        l.realtimeGoingToReconnect(DiscordGateway.this);
                    }
                });

                // And then force HeartbeatThread to reconnect on its own
                hb.state = HeartbeatState.AWAITING_WS;
                hb.lastReconnectTime = System.currentTimeMillis();
                hb.interrupt();
            }
        }

        @Override
        public void onError(Exception ex) {
            log(DEBUG_UNKNOWN_EVENTS, TAG_RX, "onError: " + ex.toString());
            if (DEBUG_UNKNOWN_EVENTS) {
                ex.printStackTrace();
            }
            // Just ignore error -- heartbeat thread will notice disconnect anyways
        }

        /**
         * Lord almighty, this is where I have sinned
         * <p>
         * This function is a workaround to run each voice listener's callback in a separate thread.
         * </p><p>
         * We have to run callbacks in a parallel (separate threads) to prevent callbacks from blocking
         * event handler thread.
         * </p>
         */
        private void foreachListener(final IForEachVoiceListener e) {
            synchronized (voiceListeners) {
                for (final IVoiceEventsListener l : voiceListeners) {
                    callbacksExecutor.submit(new Runnable() {
                        @Override
                        public void run() {
                            e.each(l);
                        }
                    });
                }
            }
        }
    }

    /**
     * This is a part of an ugly workaround
     */
    interface IForEachVoiceListener {

        void each(IVoiceEventsListener l);
    }

    enum HeartbeatState {
        /**
         * HeartbeatThread are waiting for WSClient to receive HELLO message
         */
        AWAITING_WS,
        NORMAL_HEARTBEAT,
        KILLED
    }

    private class HeartbeatThread extends Thread {

        private static final long AWAIT_THRESHOLD = 15 * 1000;
        private static final String heartbeatMessageFull = "{\"op\":1,\"d\":%d}";
        private static final String heartbeatMessageShort = "{\"op\":1,\"d\":null}";

        private static final String TAG = "Gateway/HB";

        private HeartbeatState state = HeartbeatState.AWAITING_WS;

        private int heartbeatInterval = 0;
        private long lastHeartbeatError = -1;
        private long lastReconnectTime;

        public HeartbeatThread() {
            lastReconnectTime = System.currentTimeMillis();
            start();
        }

        public void setNewHeartbeat(int heartbeatInterval) {
            log(DEBUG_HEARTBEAT, TAG, "New heartbeat interval is " + heartbeatInterval);

            this.heartbeatInterval = heartbeatInterval;
            lastAckTime = System.currentTimeMillis();

            state = HeartbeatState.NORMAL_HEARTBEAT;
            HeartbeatThread.this.interrupt();
        }

        private void reconnect() {
            log(DEBUG_HEARTBEAT, TAG + " #" + hashCode(), "Reconnecting socket");

            if (ws != null && !ws.isClosed()) ws.close();

            ws = new WSClient(URI.create(wsUrl));
            ws.connect();

            state = HeartbeatState.AWAITING_WS;
            lastReconnectTime = System.currentTimeMillis();
            HeartbeatThread.this.interrupt();
        }

        public void die() {
            state = HeartbeatState.KILLED;
            HeartbeatThread.this.interrupt();
        }

        @Override
        public void run() {
            while (true) {

                try {
                    log(DEBUG_HEARTBEAT, TAG, "State: " + state);
                    switch (state) {
                        case AWAITING_WS:
                            if (System.currentTimeMillis() < lastReconnectTime + AWAIT_THRESHOLD) sleep(10 * 1000);
                            else reconnect();
                            break;
                        case NORMAL_HEARTBEAT:
                            normalHeartbeat();
                            if (state == HeartbeatState.NORMAL_HEARTBEAT) {
                                sleep((long) (0.8f * heartbeatInterval));
                            }
                            break;
                        case KILLED:
                            return;
                    }

                } catch (InterruptedException ignored) {
                    log(DEBUG_HEARTBEAT, TAG, "Interrupted");
                }

            }
        }

        private void normalHeartbeat() {
            if (ws == null || !ws.isOpen()) onHeartbeatError();
            if (System.currentTimeMillis() - lastAckTime > heartbeatInterval) onHeartbeatError();

            if (state != HeartbeatState.NORMAL_HEARTBEAT) {
                log(DEBUG_HEARTBEAT, TAG, "We're not in NORMAL state");
                return;
            }

            try {
                if (lastSequenceNumber <= 0) ws.send(heartbeatMessageShort);
                else ws.send(String.format(heartbeatMessageFull, lastSequenceNumber));
            } catch (WebsocketNotConnectedException e) {
                onHeartbeatError();
            }

            log(DEBUG_HEARTBEAT, TAG, "Ping sent. Sq is " + lastSequenceNumber);
        }

        private void onHeartbeatError() {
            long difference = System.currentTimeMillis() - lastHeartbeatError;
            if (lastHeartbeatError > 0 && difference < 10 * 1000) dropSessionData();

            try {
                Thread.sleep(5 * 1000);
            } catch (InterruptedException ignored) {

            }

            reconnect();

            lastHeartbeatError = System.currentTimeMillis();
        }
    }

    private static final SimpleDateFormat logSdf = new SimpleDateFormat("dd.MM HH:mm:ss.SSS");

    private static void log(boolean level, String src, String message) {
        if (level) System.out.println(logSdf.format(new Date()) + " [" + src + "] " + message);
    }

}
