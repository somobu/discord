package com.iillyyaa2033.discord.v8.gateway_objs;

import com.iillyyaa2033.discord.v8.objects.GuildMember;

public class TypingStart {

    public String channel_id;
    public String guild_id;
    public String user_id;
    public long timestamp;
    public GuildMember member;

}
