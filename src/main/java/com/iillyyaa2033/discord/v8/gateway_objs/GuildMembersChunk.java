package com.iillyyaa2033.discord.v8.gateway_objs;

import com.iillyyaa2033.discord.v8.objects.GuildMember;

public class GuildMembersChunk {

    public String guild_id;
    public GuildMember[] members;
    public int chunk_index;
    public int chunk_count;
    public String[] not_found;
    public PresenceUpdate presences;

}
