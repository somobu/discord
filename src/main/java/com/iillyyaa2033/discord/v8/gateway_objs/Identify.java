package com.iillyyaa2033.discord.v8.gateway_objs;

public class Identify {

    public static final long INTENT_GUILDS = 1;
    public static final long INTENT_GUILD_MEMBERS = 1 << 1;
    public static final long INTENT_GUILD_BANS = 1 << 2;
    public static final long INTENT_GUILD_EMOJIS = 1 << 3;
    public static final long INTENT_GUILD_INTEGRATIONS = 1 << 4;
    public static final long INTENT_GUILD_WEBHOOKS = 1 << 5;
    public static final long INTENT_GUILD_INVITES = 1 << 6;
    public static final long INTENT_GUILD_STATES = 1 << 7;
    public static final long INTENT_GUILD_PRESENCES = 1 << 8;
    public static final long INTENT_GUILD_MESSAGES = 1 << 9;
    public static final long INTENT_GUILD_MESSAGE_REACTIONS = 1 << 10;
    public static final long INTENT_GUILD_MESSAGE_TYPING = 1 << 11;
    public static final long INTENT_DIRECT_MESSAGES = 1 << 12;
    public static final long INTENT_DIRECT_MESSAGE_REACTIONS = 1 << 13;
    public static final long INTENT_DIRECT_TYPING = 1 << 14;

    public static final long ALL_INTENTS = 0b11111011111101;

    public String token;
    public ConnectionProperties properties = new ConnectionProperties();
    public boolean compress = false;
    public int large_threshold = 50;
    public UpdateStatus presence = null;
    public boolean guild_subscriptions = true;
    public long intents = 0;

}
