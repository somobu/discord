package com.iillyyaa2033.discord.v8.gateway_objs;

import com.iillyyaa2033.discord.UserOnly;
import com.iillyyaa2033.discord.v8.objects.Activity;
import com.iillyyaa2033.discord.v8.objects.ClientStatus;
import com.iillyyaa2033.discord.v8.objects.User;

public class PresenceUpdate {

    public User user;
    public String status;
    public Activity[] activities;
    public ClientStatus client_status;

    public String guild_id;

    /**
     * Found this in user-only READY object
     */
    public @UserOnly long last_modified;

}
