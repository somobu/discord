package com.iillyyaa2033.discord.v8.gateway_objs;

import com.iillyyaa2033.discord.v8.objects.User;

public class GuildMemberUpdated {

    public String guild_id;
    public String[] roles;
    public User user;
    public String nick;
    public String joined_at;
    public String premium_since;
    public boolean pending;

}
