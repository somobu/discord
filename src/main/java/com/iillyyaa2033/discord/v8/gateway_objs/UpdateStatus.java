package com.iillyyaa2033.discord.v8.gateway_objs;

import com.iillyyaa2033.discord.v8.objects.Activity;
import com.iillyyaa2033.discord.v8.objects.Emoji;

public class UpdateStatus {

    public long since = System.currentTimeMillis();
    public Activity[] activities = {};
    public String status = "online";
    public boolean afk = false;

    public static UpdateStatus customStatus(String type, boolean afk, long since, String text, Emoji emoji) {
        UpdateStatus status = new UpdateStatus();
        status.status = type;
        status.afk = afk;
        status.since = since;

        Activity activity = new Activity();
        activity.type = 4;
        activity.name = text;
        activity.emoji = emoji;

        status.activities = new Activity[]{activity};

        return status;
    }

    public static UpdateStatus unknownGameStatus(long since, String name){
        UpdateStatus status = new UpdateStatus();
        status.status = "online";
        status.afk = true;
        status.since = since;

        Activity activity = new Activity();
        activity.type = 0;
        activity.name = name;
        Activity.Timestamp t = new Activity.Timestamp();
        t.start = since;
        activity.timestamps = new Activity.Timestamp[]{t};

        status.activities = new Activity[]{activity};

        return status;
    }
}
