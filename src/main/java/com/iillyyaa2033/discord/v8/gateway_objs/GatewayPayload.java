package com.iillyyaa2033.discord.v8.gateway_objs;

import com.google.gson.JsonElement;

public class GatewayPayload {

    public int op;
    public JsonElement d;
    public int s = -1;
    public String t = null;

}
