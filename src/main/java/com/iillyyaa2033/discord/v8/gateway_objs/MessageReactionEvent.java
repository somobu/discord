package com.iillyyaa2033.discord.v8.gateway_objs;

import com.iillyyaa2033.discord.v8.objects.Emoji;
import com.iillyyaa2033.discord.v8.objects.GuildMember;

public class MessageReactionEvent {

    public String user_id;
    public String channel_id;
    public String message_id;
    public String guild_id;

    /**
     * Only when reaction created
     */
    public GuildMember member;

    public Emoji emoji;

}
