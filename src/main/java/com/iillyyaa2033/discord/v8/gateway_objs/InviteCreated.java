package com.iillyyaa2033.discord.v8.gateway_objs;

import com.iillyyaa2033.discord.v8.objects.User;

public class InviteCreated {

    public String channel_id;
    public String code;
    public String created_at;
    public String guild_id;
    public User inviter;
    public int max_age;
    public int max_uses;
    public User target_user;
    public int target_user_type;
    public boolean temporary;
    public int uses;

}
