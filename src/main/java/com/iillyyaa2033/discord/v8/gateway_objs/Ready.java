package com.iillyyaa2033.discord.v8.gateway_objs;

import com.google.gson.JsonArray;
import com.iillyyaa2033.discord.BotOnly;
import com.iillyyaa2033.discord.UserOnly;
import com.iillyyaa2033.discord.v8.objects.*;

import java.util.Map;

public class Ready {

    public int v;

    public User user;

    public String session_id;

    /**
     * When connecting as bot, it is GuildUnavailable object
     */
    public Guild[] guilds;

    public @BotOnly int[] shard;

    public @BotOnly Application application;

    public @UserOnly String user_settings_proto;

    public @UserOnly UserSettings user_settings;

    public @UserOnly Session[] sessions;

    public @UserOnly String session_type;

    public @UserOnly Relationship[] relationships;

    public @UserOnly ReadState[] read_state;

    public @UserOnly Channel[] private_channels;

    public @UserOnly PresenceUpdate[] presences;

    public @UserOnly Map<String, String> notes;

    public @UserOnly String[] geo_ordered_rtc_regions;

    public @UserOnly int friend_suggestion_count;

    public @UserOnly String country_code;

    public @UserOnly Connection[] connected_accounts;

    public @UserOnly String analytics_token;

    public @UserOnly JsonArray guild_join_requests;

    /**
     * WTF is that?
     */
    public @UserOnly JsonArray user_guild_settings;

    /**
     * WTF is that?
     */
    public @UserOnly JsonArray experiments, guild_experiments;

}
