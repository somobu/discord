package com.iillyyaa2033.discord.v8.objects;

public class InteractionCallbackData {

    public boolean tts;
    public String content;
    public Embed[] embeds;
    public AllowedMentions allowed_mentions;
    public int flags;
    public MessageComponent components;

}
