package com.iillyyaa2033.discord.v8.objects;

public class VoiceState {

    public String guild_id, channel_id, user_id;
    public GuildMember member;
    public String session_id;
    public boolean deaf, mute, self_deaf, self_mute, self_stream, self_video, suppress;
    public String request_to_speak_timestamp;

}
