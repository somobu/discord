package com.iillyyaa2033.discord.v8.objects;

import com.iillyyaa2033.discord.UserOnly;

@UserOnly
public class ReadState {

    public String id;
    public int mention_count;
    public String last_pin_timestamp;
    public String last_message_id;

}
