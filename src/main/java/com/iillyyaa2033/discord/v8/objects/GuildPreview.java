package com.iillyyaa2033.discord.v8.objects;

public class GuildPreview {

    public String id, name, icon, splash, discovery_splash;
    public Emoji[] emojis;
    public String[] features;
    public int approximate_member_count, approximate_presence_count;
    public String description;

}
