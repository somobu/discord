package com.iillyyaa2033.discord.v8.objects;

public class Message {

    public String id, channel_id, guild_id;
    public User author;
    public GuildMember member;
    public String content, timestamp, edited_timestamp;
    public boolean tts, mention_everyone;
    public User[] mentions;
    public String[] mention_roles;
    public ChannelMention mention_channels;
    public Attachment[] attachments;
    public Embed[] embeds;
    public Reaction[] reactions;
    public String nonce;
    public boolean pinned, webhook_id;
    public int type;
    public MessageActivity activity;
    public Application application;
    public MessageReference message_reference;
    public int flags;
    public Sticker stickers;
    public Message referenced_message;
    public Interaction interaction;

}
