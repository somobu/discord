package com.iillyyaa2033.discord.v8.objects;

public class EmbedAuthor {

    public String name;
    public String url;
    public String icon_url;
    public String proxy_icon_url;

}
