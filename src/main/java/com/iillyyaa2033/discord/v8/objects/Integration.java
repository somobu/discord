package com.iillyyaa2033.discord.v8.objects;

public class Integration {

    public String id;
    public String name;
    public String type;
    public boolean enabled;
    public boolean syncing;
    public String role_id;
    public String enable_emoticons;
    public String expire_behavior;
    public String expire_grace_period;
    public User user;
    public IntegrationAccount account;
    public String synced_at;
    public int subscriber_count;
    public boolean revoked;
    public Application application;

}
