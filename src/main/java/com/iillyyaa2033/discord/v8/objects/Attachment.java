package com.iillyyaa2033.discord.v8.objects;

public class Attachment {

    public String id, filename, content_type;
    public int size;
    public String url, proxy_url;
    public int height, width;

}
