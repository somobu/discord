package com.iillyyaa2033.discord.v8.objects;

public class EmbedGenericMedia {

    public String url;
    public String proxy_url;
    public int height;
    public int width;

}
