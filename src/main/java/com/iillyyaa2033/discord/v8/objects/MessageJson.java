package com.iillyyaa2033.discord.v8.objects;

/**
 * Use this to send message with json-only content
 */
public class MessageJson {

    public String content;
    public String nonce = null;
    public boolean tts = false;
    public Embed embed = null;
    public AllowedMentions[] allowed_mentions = null;
    public MessageReference message_reference = null;

}
