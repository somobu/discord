package com.iillyyaa2033.discord.v8.objects;

public class GuildMember {

    public User user;
    public String nick;
    public String[] roles;
    public String joined_at;
    public String premium_since;
    public boolean deaf;
    public boolean mute;
    public boolean pending;
    public String permissions;
    public String hoisted_role;

    /**
     * Sent on realtime only
     */
    public String guild_id;
}
