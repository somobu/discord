package com.iillyyaa2033.discord.v8.objects;

public class Invite {

    public String code;
    public Guild guild;
    public Channel channel;
    public User inviter;
    public User target_user;
    public int target_user_type;
    public int approximate_presence_count;
    public int approximate_member_count;

}
