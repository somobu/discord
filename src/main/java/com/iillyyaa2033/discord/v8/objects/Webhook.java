package com.iillyyaa2033.discord.v8.objects;

public class Webhook {

    public String id;
    public int type;
    public String guild_id, channel_id;
    public User user;
    public String name, avatar, token, application_id;
    public Guild source_guild;
    public Channel source_channel;
    public String url;

}
