package com.iillyyaa2033.discord.v8;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.iillyyaa2033.discord.MPFL;
import com.iillyyaa2033.discord.v8.objects.*;
import com.iillyyaa2033.utils.NetworkUtils;
import com.iillyyaa2033.utils.StreamUtils;

import javax.net.ssl.SSLProtocolException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class DiscordHttp {

    private static final String endpointUrl = "https://discord.com/api/v8/";
    private static final int DEFAULT_RETRY_LIMIT = 3;
    private static final int MESSAGE_SYMBOLS_LIMIT = 2000;

    private static final String DELETE = "DELETE";
    private static final String GET = "GET";
    private static final String PATCH = "PATCH";
    private static final String POST = "POST";
    private static final String PUT = "PUT";

    public static class Settings {

        private static final String BOT_USER_AGENT = "DiscordBot (%s, %s)";

        private static final String DEFAULT_BOT_URL = "somobu-lib";
        private static final String DEFAULT_BOT_VER = "1.0";

        public boolean isBot;
        public String token;

        public String userAgent;

        String proxy = null;

        public Settings(String token, boolean isBot) {
            this.token = token;
            this.isBot = isBot;
            userAgent = formatBotUserAgent(DEFAULT_BOT_URL, DEFAULT_BOT_VER);
        }

        /**
         * In case you are need to format your own bot user agent
         *
         * @param url     URL of your bot
         * @param version your bot ver
         * @return proper user agent string
         */
        public static String formatBotUserAgent(String url, String version) {
            return String.format(BOT_USER_AGENT, url, version);
        }

        boolean isUsingProxy() {
            if (proxy == null) {
                return false;
            } else {
                return !proxy.isEmpty();
            }
        }

        String getProxyHost() {
            if (proxy != null) {
                return proxy.split(":")[0];
            } else {
                return null;
            }
        }

        int getProxyPort() {
            try {
                return Integer.parseInt(proxy.split(":")[1]);
            } catch (Exception e) {
                return 8080;
            }
        }

        String getProperToken() {
            if (token == null) throw new RuntimeException("Token is not set");

            if (isBot) return "Bot " + token;
            else return token;
        }
    }

    private final Gson gson = new GsonBuilder().create();
    private final Settings settings;

    /**
     * Default constructor for bots
     */
    public DiscordHttp(String token) {
        settings = new Settings(token, true);
    }

    /**
     * Constructor with customized settings
     */
    public DiscordHttp(Settings settings) {
        this.settings = settings;
    }


    // ====================================== //
    //                INTERNAL                //
    // ====================================== //

    private String intf(String method, String format, Object... args) throws IOException {
        return internalHttpRequest(method, String.format(format, args));
    }

    private String internalHttpRequest(final String method, final String sub_path) throws IOException {
        return internalHttpRequest(method, sub_path, null, DEFAULT_RETRY_LIMIT, true);
    }

    private String internalHttpRequest(final String method, final String sub_path, String data) throws IOException {
        return internalHttpRequest(method, sub_path, data, DEFAULT_RETRY_LIMIT, true);
    }

    private String internalHttpRequest(final String method, final String sub_path, final String data, int retryLimit, boolean needAnswer) throws IOException {
        return internalHttpRequest(method, sub_path, data, retryLimit, needAnswer, true);
    }

    private String internalHttpRequest(final String method, final String sub_path, final String data, int retryLimit, boolean needAnswer, boolean needAuthorization) throws IOException {

        boolean needOutput = (data != null);

        byte[] bytes = new byte[0];

        String url = endpointUrl + sub_path;
        HttpURLConnection http;

        if (settings.isUsingProxy()) {
            Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(settings.getProxyHost(), settings.getProxyPort()));
            http = (HttpURLConnection) new URL(url).openConnection(proxy);
        } else {
            http = (HttpURLConnection) new URL(url).openConnection();
        }

        http.setRequestMethod(method);

        // Set header fields
        if (!method.toLowerCase().equals("delete"))
            http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        http.setRequestProperty("User-Agent", settings.userAgent);

        if (needAuthorization) http.setRequestProperty("Authorization", settings.getProperToken());

        http.setDoOutput(needOutput);
        http.setDoInput(true);

        if (needOutput) {
            bytes = data.getBytes(StandardCharsets.UTF_8);
            http.setFixedLengthStreamingMode(bytes.length);
        }

        try {
            http.connect();
        } catch (IOException e) {
            if (retryLimit > 0) {
                System.out.println("Got error while connecting: " + e.getMessage() + ". Retrying...");
                return internalHttpRequest(method, sub_path, data, retryLimit - 1, needAnswer);
            } else {
                throw e;
            }
        }

        if (needOutput) {
            try (OutputStream os = http.getOutputStream()) {
                os.write(bytes);
                os.flush();
            }
        }

        if (needAnswer) {
            int rc;
            try {
                rc = http.getResponseCode();
            } catch (SSLProtocolException e) {
                if (retryLimit > 0) {
                    System.out.println("Got error while reading respose code: " + e.getMessage() + ". Retrying...");
                    return internalHttpRequest(method, sub_path, data, retryLimit - 1, needAnswer);
                } else {
                    throw e;
                }
            }

            // Reading server's answer
            if (rc == 204) { // 204 is an empty response code
                return "";
            } else if (200 <= rc && rc <= 299) {
                return NetworkUtils.fasterRead(http);
            } else {
                String serverAnswer;

                try {
                    InputStream errorStream = http.getErrorStream();
                    serverAnswer = StreamUtils.convertStreamToString(errorStream);
                } catch (Exception e) {
                    serverAnswer = "http code " + rc;
                }

                throw new IOException(method + " " + sub_path + " " + serverAnswer);
            }
        } else {
            return null;
        }
    }

    private String urlEncode(String value) {
        try {
            return URLEncoder.encode(value, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    // ====================================== //
    //                 CHANNEL                //
    // ====================================== //

    // More methods here

    public Channel getChannel(String channelId) throws IOException {
        return gson.fromJson(intf(GET, "/channels/%s", channelId), Channel.class);
    }

    public Channel modifyChannel(String channelId, ChannelModification mod) throws IOException {
        String rz = internalHttpRequest(PATCH, String.format("/channels/%s", channelId), gson.toJson(mod));
        return gson.fromJson(rz, Channel.class);
    }

    public Channel deleteChannel(String channelId) throws IOException {
        return gson.fromJson(intf(DELETE, "/channels/%s", channelId), Channel.class);
    }

    public Message[] getChannelMessages(String channelId, String around, String before, String after, int limit) throws IOException {
        String subpath = String.format("/channels/%s/messages", channelId);

        subpath += "?limit=" + limit;
        if (around != null) subpath += "&around=" + around;
        if (before != null) subpath += "&before=" + before;
        if (after != null) subpath += "&after=" + after;

        String rz = internalHttpRequest(GET, subpath);
        return gson.fromJson(rz, Message[].class);
    }

    public Message getChannelMessage(String channelId, String messageId) throws IOException {
        return gson.fromJson(intf(GET, "/channels/%s/messages/%s", channelId, messageId), Message.class);
    }

    public Message createMessage(String channelId, String text) throws IOException {
        MessageJson json = new MessageJson();
        json.content = text;
        return createMessage(channelId, json);
    }

    public Message createMessage(String channelId, MessageJson json) throws IOException {
        String rz = internalHttpRequest(POST, String.format("/channels/%s/messages", channelId), gson.toJson(json));
        return gson.fromJson(rz, Message.class);
    }

    public Message createMessage(String channelId, MessageMultipart msg, String path) throws IOException {
        URL url = new URL(String.format("%s/channels/%s/messages", endpointUrl, channelId));

        // Setting up connection
        HttpURLConnection httpConn;
        if (settings.isUsingProxy()) {
            Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(settings.getProxyHost(), settings.getProxyPort()));
            httpConn = (HttpURLConnection) url.openConnection(proxy);
        } else {
            httpConn = (HttpURLConnection) url.openConnection();
        }

        // Set header fields
        httpConn.setRequestProperty("User-Agent", settings.userAgent);
        httpConn.setRequestProperty("Authorization", settings.getProperToken());

        // SEND FILE
        MPFL.multipartFileUpload(httpConn, gson.toJson(msg), "file", path, null);

        // CHECK STATUS
        int status = httpConn.getResponseCode();
        if (status != HttpURLConnection.HTTP_OK) {
            throw new IOException("Server returned non-OK status: " + status);
        } else {
            String serverAnswer = NetworkUtils.fasterRead(httpConn);
            return gson.fromJson(serverAnswer, Message.class);
        }
    }

    public Message crosspostMessage(String channelId, String messageId) throws IOException {
        return gson.fromJson(intf(POST, "/channels/%s/messages/%s/crosspost", channelId, messageId), Message.class);
    }

    /**
     * @param emoji can be utf emoji, emoji id (`700077489275535501`) or discord's emoji name (`crycat:700077489275535501`)
     */
    public void createReaction(String channelId, String messageId, String emoji) throws IOException {
        if (emoji.length() > 5 && !emoji.contains(":")) emoji = "a:" + emoji; // Prefix emoji ids with simple emoji name
        emoji = urlEncode(emoji);   // Url-encode emoji

        String subpath = String.format("/channels/%s/messages/%s/reactions/%s/@me", channelId, messageId, emoji);
        internalHttpRequest(PUT, subpath, "");
    }

    public void deleteOwnReaction(String channelId, String messageId, String emoji) throws IOException {
        // TODO: percent-encode emoji
        intf(DELETE, "/channels/%s/messages/%s/reactions/%s/@me", channelId, messageId, emoji);
    }

    public void deleteUserReaction(String channelId, String messageId, String emoji, String userId) throws IOException {
        // TODO: percent-encode emoji
        intf(DELETE, "/channels/%s/messages/%s/reactions/%s/%s", channelId, messageId, emoji, userId);
    }

    public User[] getReactions(String channelId, String messageId, String emoji, String before, String after, int limit) throws IOException {
        // TODO: percent-encode emoji
        String subpath = String.format("/channels/%s/messages/%s/reactions/%s", channelId, messageId, emoji);

        JsonObject json = new JsonObject();
        json.addProperty("before", before);
        json.addProperty("after", after);
        json.addProperty("limit", limit);

        String rz = internalHttpRequest(GET, subpath, json.toString());
        return gson.fromJson(rz, User[].class);
    }

    public void deleteAllReactions(String channelId, String messageId) throws IOException {
        intf(DELETE, "/channels/%s/messages/%s/reactions", channelId, messageId);
    }

    public void deleteAllReactionsForEmoji(String channelId, String messageId, String emoji) throws IOException {
        // TODO: percent-encode emoji
        intf(DELETE, "/channels/%s/messages/%s/reactions/%s", channelId, messageId, emoji);
    }

    public Message editMessage(String channelId, String messageId, String content, Embed embed, long flags,
                               AllowedMentions allowedMentions) throws IOException {
        String subpath = String.format("/channels/%s/messages/%s", channelId, messageId);

        JsonObject json = new JsonObject();
        if (content != null) json.addProperty("content", content);
        if (embed != null) json.add("embed", gson.toJsonTree(embed));
        if (flags > 0) json.addProperty("flags", flags);
        if (allowedMentions != null) json.add("allowed_mentions", gson.toJsonTree(allowedMentions));

        String rz = internalHttpRequest(PATCH, subpath, json.toString());
        return gson.fromJson(rz, Message.class);
    }

    public void deleteMessage(String channelId, String messageId) throws IOException {
        intf(DELETE, "/channels/%s/messages/%s", channelId, messageId);
    }

    public void bulkDeleteMessages(String channelId, String[] ids) throws IOException {
        String subpath = String.format("/channels/%s/messages/bulk-delete", channelId);
        JsonObject json = new JsonObject();
        json.add("messages", gson.toJsonTree(ids));
        internalHttpRequest(POST, subpath, json.toString());
    }

    public void editChannelPermissions(String channelId, String overwriteId, String allow, String deny, int type) throws IOException {
        String subpath = String.format("/channels/%s/permissions/%s", channelId, overwriteId);
        String data = String.format("{\"allow\":\"%s\",\"deny\":\"%s\",\"type\":%d}", allow, deny, type);
        internalHttpRequest(PUT, subpath, data);
    }

    public Invite[] getChannelInvites(String channelId) throws IOException {
        return gson.fromJson(intf(GET, "/channels/%s/invites", channelId), Invite[].class);
    }

    public Invite createChannelInvite(String channelId, int maxAge, int maxUses, boolean temporary, boolean unique,
                                      String targetUserId, int targetUserType) throws IOException {
        String subpath = String.format("/channels/%s/invites", channelId);

        String data;
        if (targetUserId == null)
            data = String.format("{\"max_age\":%d,\"max_uses\":%d,\"temporary\":\"%s\",\"unique\":\"%s\"}",
                    maxAge, maxUses, "" + temporary, "" + unique);
        else
            data = String.format("{\"max_age\":%d,\"max_uses\":%d,\"temporary\":%s,\"unique\":%s,\"target_user\":%s,\"target_user_type\":%d}",
                    maxAge, maxUses, "" + temporary, "" + unique, targetUserId, targetUserType);

        return gson.fromJson(internalHttpRequest(POST, subpath, data), Invite.class);
    }

    public void deleteChannelPermission(String channelId, String overwriteId) throws IOException {
        intf(POST, "/channels/%s/permissions/%s", channelId, overwriteId);
    }

    public FollowedChannel followNewsChannel(String channelId, String webhook_channel_id) throws IOException {
        String subpath = String.format("/channels/%s/followers", channelId);
        String data = String.format("{\"webhook_channel_id\":\"%s\"}", webhook_channel_id);
        String result = internalHttpRequest(POST, subpath, data);
        return gson.fromJson(result, FollowedChannel.class);
    }

    public void triggerTypingIndicator(String channelId) throws IOException {
        intf(POST, "/channels/%s/typing", channelId);
    }

    public Message[] getPinnedMessages(String channelId, String messageId) throws IOException {
        return gson.fromJson(intf(GET, "/channels/%s/pins", channelId, messageId), Message[].class);
    }

    public void addPinnedMessage(String channelId, String messageId) throws IOException {
        intf(PUT, "/channels/%s/pins/%s", channelId, messageId);
    }

    public void deletePinnedMessage(String channelId, String messageId) throws IOException {
        intf(DELETE, "/channels/%s/pins/%s", channelId, messageId);
    }

    public void groupDmAddRecipient(String channelId, String userId) throws IOException {
        intf(PUT, "/channels/%s/recipients/%s", channelId, userId);
    }

    public void groupDmRemoveRecipient(String channelId, String userId) throws IOException {
        intf(DELETE, "/channels/%s/recipients/%s", channelId, userId);
    }


    // ====================================== //
    //                 EMOJI                  //
    // ====================================== //

    public Emoji[] listGuildEmojis(String guildId) throws IOException {
        String rz = intf(GET, "/guilds/%s/emojis", guildId);
        return gson.fromJson(rz, Emoji[].class);
    }

    public Emoji getGuildEmoji(String guildId, String emojiId) throws IOException {
        String rz = intf(GET, "/guilds/%s/emojis/%s", guildId, emojiId);
        return gson.fromJson(rz, Emoji.class);
    }

    public Emoji createGuildEmoji(String guildId, String name, ImageData image, String[] roles) throws IOException {
        String subpath = String.format("/guilds/%s/emojis", guildId);

        JsonObject json = new JsonObject();
        json.addProperty("name", name);
        json.addProperty("image", image.toString());
        json.add("roles", gson.toJsonTree(roles));

        String rz = internalHttpRequest(POST, subpath, json.toString());
        return gson.fromJson(rz, Emoji.class);
    }

    public Emoji modifyGuildEmoji(String guildId, String emojiId, String name, String[] roles) throws IOException {
        String subpath = String.format("/guilds/%s/emojis/%s", guildId, emojiId);

        JsonObject json = new JsonObject();
        json.addProperty("name", name);
        json.add("roles", gson.toJsonTree(roles));

        String rz = internalHttpRequest(PATCH, subpath, json.toString());
        return gson.fromJson(rz, Emoji.class);
    }

    public void deleteGuildEmoji(String guildId, String emojiId) throws IOException {
        intf(DELETE, "/guilds/%s/emojis/%s", guildId, emojiId);
    }


    // ====================================== //
    //                 GUILD                  //
    // ====================================== //

    public Guild createGuild(GuildCreate data) throws IOException {
        String rz = internalHttpRequest(POST, "/guilds", gson.toJson(data));
        return gson.fromJson(rz, Guild.class);
    }

    public Guild getGuild(String guildId) throws IOException {
        // TODO: query params
        return gson.fromJson(intf(GET, "/guilds/%s", guildId), Guild.class);
    }

    public GuildPreview getGuildPreview(String guildId) throws IOException {
        return gson.fromJson(intf(GET, "/guilds/%s/preview", guildId), GuildPreview.class);
    }

    public Guild modifyGuild(String guildId, GuildModify data) throws IOException {
        String rz = internalHttpRequest(PATCH, String.format("/guilds/%s", guildId), gson.toJson(data));
        return gson.fromJson(rz, Guild.class);
    }

    public void deleteGuild(String guildId) throws IOException {
        intf(DELETE, "/guilds/%s", guildId);
    }

    public Channel[] getGuildChannels(String guildId) throws IOException {
        return gson.fromJson(intf(GET, "/guilds/%s/channels", guildId), Channel[].class);
    }

    public void createGuildChannel() {
        // TODO: implement me
    }

    /**
     * @param positions map 'channel id - channel position'
     */
    public void modifyGuildChannelPositions(String guildId, Map<String, Integer> positions) throws IOException {
        String url = String.format("/guilds/%s/channels", guildId);

        JsonArray json = new JsonArray();
        for (String key : positions.keySet()) {
            JsonObject item = new JsonObject();
            item.addProperty("id", key);
            item.addProperty("position", positions.get(key));
            json.add(item);
        }

        internalHttpRequest(PATCH, url, json.toString());
    }

    public GuildMember getGuildMember(String guildId, String userId) throws IOException {
        return gson.fromJson(intf(GET, "/guilds/%s/members/%s", guildId, userId), GuildMember.class);
    }

    public GuildMember[] listGuildMembers(String guildId) throws IOException {
        // TODO: query params
        return gson.fromJson(intf(GET, "/guilds/%s/members", guildId), GuildMember[].class);
    }

    public void addGuildMember() {
        // TODO: implement me
    }

    public GuildMember modifyGuildMember(String guildId, String userId, String nick, String[] roles, boolean mute,
                                         boolean deaf, String channelId) throws IOException {
        String url = String.format("/guilds/%s/members/%s", guildId, userId);

        JsonObject json = new JsonObject();
        json.addProperty("nick", nick);
        json.add("roles", gson.toJsonTree(roles));
        json.addProperty("mute", mute);
        json.addProperty("deaf", deaf);
        json.addProperty("channel_id", channelId);

        return gson.fromJson(internalHttpRequest(PATCH, url, json.toString()), GuildMember.class);
    }

    public void modifyCurrentUserNick(String guildId, String nick) throws IOException {
        String url = String.format("/guilds/%s/members/@me/nick", guildId);

        JsonObject json = new JsonObject();
        json.addProperty("nick", nick);

        internalHttpRequest(PATCH, url, json.toString());
    }

    public void addGuildMemberRole(String guildId, String userId, String roleId) throws IOException {
        internalHttpRequest(PUT, String.format("/guilds/%s/members/%s/roles/%s", guildId, userId, roleId), "");
    }

    public void removeGuildMemberRole(String guildId, String userId, String roleId) throws IOException {
        internalHttpRequest(DELETE, String.format("/guilds/%s/members/%s/roles/%s", guildId, userId, roleId), "");
    }

    public void removeGuildMember(String guildId, String userId) throws IOException {
        intf(DELETE, "/guilds/%s/members/%s", guildId, userId);
    }

    public Ban[] getGuildBans(String guildId) throws IOException {
        return gson.fromJson(intf(GET, "/guilds/%s/bans", guildId), Ban[].class);
    }

    public Ban getGuildBan(String guildId, String userId) throws IOException {
        return gson.fromJson(intf(GET, "/guilds/%s/bans/%s", guildId, userId), Ban.class);
    }

    public void createGuildBan(String guildId, String userId, int days, String reason) throws IOException {
        String url = String.format("/guilds/%s/bans/%s", guildId, userId);

        JsonObject json = new JsonObject();
        json.addProperty("delete_message_days", days);
        json.addProperty("reason", reason);

        internalHttpRequest(PUT, url, json.toString());
    }

    public void removeGuildBan(String guildId, String userId) throws IOException {
        intf(DELETE, "/guilds/%s/bans/%s", guildId, userId);
    }

    public Role[] getGuildRoles(String guildId) throws IOException {
        return gson.fromJson(intf(GET, "/guilds/%s/roles", guildId), Role[].class);
    }

    public Role createGuildRole(String guildId, String name, String permissions, int color, boolean hoist,
                                boolean mentionable) throws IOException {
        String url = String.format("/guilds/%s/roles", guildId);

        JsonObject json = new JsonObject();
        json.addProperty("name", name);
        json.addProperty("permissions", permissions);
        json.addProperty("color", color);
        json.addProperty("hoist", hoist);
        json.addProperty("mentionable", mentionable);

        String rz = internalHttpRequest(POST, url, json.toString());
        return gson.fromJson(rz, Role.class);
    }

    public Role[] modifyGuildRolePositions(String guildId, Map<String, Integer> positions) throws IOException {
        String url = String.format("/guilds/%s/roles", guildId);

        JsonArray json = new JsonArray();
        for (String key : positions.keySet()) {
            JsonObject item = new JsonObject();
            item.addProperty("id", key);
            item.addProperty("position", positions.get(key));
            json.add(item);
        }

        String rz = internalHttpRequest(PATCH, url, json.toString());
        return gson.fromJson(rz, Role[].class);
    }

    public Role modifyGuildRole(String guildId, String roleId, String name, String permissions, int color,
                                boolean hoist, boolean mentionable) throws IOException {
        String url = String.format("/guilds/%s/roles/%s", guildId, roleId);

        JsonObject json = new JsonObject();
        json.addProperty("name", name);
        json.addProperty("permissions", permissions);
        json.addProperty("color", color);
        json.addProperty("hoist", hoist);
        json.addProperty("mentionable", mentionable);

        String rz = internalHttpRequest(PATCH, url, json.toString());
        return gson.fromJson(rz, Role.class);
    }

    public void deleteGuildRole(String guildId, String roleId) throws IOException {
        intf(DELETE, "/guilds/%s/roles/%s", guildId, roleId);
    }

    public void getGuildPruneCount(String guildId) {
        // TODO: what should I do here?
    }

    public void beginGuildPrune(String guildId, int days, boolean computePruneCount, String[] includeRoles) throws IOException {
        String url = String.format("/guilds/%s/prune", guildId);

        JsonObject json = new JsonObject();
        json.addProperty("days", days);
        json.addProperty("compute_prune_count", computePruneCount);
        json.add("include_roles", gson.toJsonTree(includeRoles));

        internalHttpRequest(POST, url, json.toString());

        // TODO: what should I return here?
    }

    public VoiceRegion[] getGuildVoiceRegions(String guildId) throws IOException {
        return gson.fromJson(intf(GET, "/guilds/%s/regions", guildId), VoiceRegion[].class);
    }

    public Invite[] getGuildInvites(String guildId) throws IOException {
        return gson.fromJson(intf(GET, "/guilds/%s/invites", guildId), Invite[].class);
    }

    public Integration[] getGuildIntegrations(String guildId) throws IOException {
        return gson.fromJson(intf(GET, "/guilds/%s/integrations", guildId), Integration[].class);
    }

    public void createGuildIntegration(String guildId, String type, String integrationId) throws IOException {
        String url = String.format("/guilds/%s/integrations", guildId);

        JsonObject json = new JsonObject();
        json.addProperty("type", type);
        json.addProperty("id", integrationId);

        internalHttpRequest(POST, url, json.toString());
    }

    public void modifyGuildIntegration(String guildId, String integrationId, int expireBehavior, int expireGracePeriod,
                                       boolean enableEmoticons) throws IOException {
        String url = String.format("/guilds/%s/integrations/%s", guildId, integrationId);

        JsonObject json = new JsonObject();
        json.addProperty("expire_behavior", expireBehavior);
        json.addProperty("expire_grace_period", expireGracePeriod);
        json.addProperty("enable_emoticons", enableEmoticons);

        internalHttpRequest(PATCH, url, json.toString());
    }

    public void deleteGuildIntegration(String guildId, String integrationId) throws IOException {
        intf(DELETE, "/guilds/%s/integrations/%s/sync", guildId, integrationId);
    }

    public void syncGuildIntegration(String guildId, String integrationId) throws IOException {
        intf(POST, "/guilds/%s/integrations/%s/sync", guildId, integrationId);
    }

    public GuildWidgetSettings getGuildWidgetSettings(String guildId) throws IOException {
        return gson.fromJson(intf(GET, "/guilds/%s/widget", guildId), GuildWidgetSettings.class);
    }

    public GuildWidgetSettings modifyGuildWidget(String guildId, GuildWidgetSettings settings) throws IOException {
        String url = String.format("/guilds/%s/widget", guildId);
        String data = gson.toJson(settings);
        String rz = internalHttpRequest(PATCH, url, data);
        return gson.fromJson(rz, GuildWidgetSettings.class);
    }

    public GuildWidgetJson getGuildWidget(String guildId) throws IOException {
        return gson.fromJson(intf(GET, "/guilds/%s/widget.json", guildId), GuildWidgetJson.class);
    }

    public Invite getGuildVanityUrl(String guildId) throws IOException {
        return gson.fromJson(intf(GET, "/guilds/%s/vanity-url", guildId), Invite.class);
    }


    // ====================================== //
    //                 INVITE                 //
    // ====================================== //

    public Invite getInvite(String code, boolean withCounts) throws IOException {
        String rz = intf(GET, "/invites/%s?with_counts=%s", code, "" + withCounts);
        return gson.fromJson(rz, Invite.class);
    }

    public Invite deleteInvite(String code) throws IOException {
        String rz = intf(GET, "/invites/%s", code);
        return gson.fromJson(rz, Invite.class);
    }


    // ====================================== //
    //                TEMPLATE                //
    // ====================================== //

    public Template getTemplate(String code) throws IOException {
        String rz = intf(GET, "/guilds/templates/%s", code);
        return gson.fromJson(rz, Template.class);
    }

    public Guild createGuildFromTemplate(String code, String name, ImageData icon) throws IOException {
        String subpath = String.format("/guilds/templates/%s", code);

        JsonObject json = new JsonObject();
        json.addProperty("name", name);
        json.addProperty("icon", icon.toString());

        String rz = internalHttpRequest(POST, subpath, json.toString());
        return gson.fromJson(rz, Guild.class);
    }

    public Template getGuildTemplates(String guildId) throws IOException {
        String rz = intf(GET, "/guilds/%s/templates", guildId);
        return gson.fromJson(rz, Template.class);
    }

    public Template createGuildTemplate(String guildId, String name, String description) throws IOException {
        String subpath = String.format("/guilds/%s/templates", guildId);

        JsonObject json = new JsonObject();
        json.addProperty("name", name);
        json.addProperty("description", description);

        String rz = internalHttpRequest(POST, subpath, json.toString());
        return gson.fromJson(rz, Template.class);
    }

    public Template syncGuildTemplate(String guildId, String code) throws IOException {
        String rz = intf(PUT, "/guilds/%s/templates/%s", guildId, code);
        return gson.fromJson(rz, Template.class);
    }

    public Template modifyGuildTemplate(String guildId, String code, String name, String description) throws IOException {
        String subpath = String.format("/guilds/%s/templates/%s", guildId, code);

        JsonObject json = new JsonObject();
        json.addProperty("name", name);
        json.addProperty("description", description);

        String rz = internalHttpRequest(PATCH, subpath, json.toString());
        return gson.fromJson(rz, Template.class);
    }

    public Template deleteGuildTemplate(String guildId, String code) throws IOException {
        String rz = intf(DELETE, "/guilds/%s/templates/%s", guildId, code);
        return gson.fromJson(rz, Template.class);
    }


    // ====================================== //
    //                  USER                  //
    // ====================================== //

    public User getCurrentUser() throws IOException {
        return getUser("@me");
    }

    public User getUser(String userId) throws IOException {
        return gson.fromJson(intf(GET, "/users/%s", userId), User.class);
    }

    public User modifyCurrentUser(String name, ImageData avatar) throws IOException {
        String subpath = "/users/@me";

        JsonObject json = new JsonObject();
        json.addProperty("username", name);
        json.addProperty("avatar", avatar.toString());

        String rz = internalHttpRequest(PATCH, subpath, json.toString());
        return gson.fromJson(rz, User.class);
    }

    public Guild[] getCurrentUserGuilds() throws IOException {
        // TODO: query parameters
        return gson.fromJson(intf(GET, "/users/@me/guilds/"), Guild[].class);
    }

    public void leaveGuild(String guildId) throws IOException {
        intf(DELETE, "/users/@me/guilds/%s", guildId);
    }

    public Connection[] getUserDMs() throws IOException {
        return gson.fromJson(intf(GET, "/users/@me/channels"), Connection[].class);
    }

    public Channel createDM(String recipientId) throws IOException {
        String subpath = "/users/@me/channels";

        JsonObject json = new JsonObject();
        json.addProperty("recipient_id", recipientId);

        String rz = internalHttpRequest(POST, subpath, json.toString());
        return gson.fromJson(rz, Channel.class);
    }

    public Channel createGroupDM(String[] accessTokens, Map<String, String> nicks) throws IOException {
        String subpath = "/users/@me/channels";

        JsonObject json = new JsonObject();
        json.add("access_tokens", gson.toJsonTree(accessTokens));
        json.add("nicks", gson.toJsonTree(nicks));

        String rz = internalHttpRequest(POST, subpath, json.toString());
        return gson.fromJson(rz, Channel.class);
    }

    public Connection[] getUserConnections() throws IOException {
        return gson.fromJson(intf(GET, "/users/@me/connections"), Connection[].class);
    }


    // ====================================== //
    //                COMMANDS                //
    // ====================================== //

    public ApplicationCommand[] getGuildApplicationCommands(String applicationId, String guildId) throws IOException {
        String subpath = String.format("/applications/%s/guilds/%s/commands", applicationId, guildId);
        String rz = internalHttpRequest(GET, subpath);
        return gson.fromJson(rz, ApplicationCommand[].class);
    }

    public ApplicationCommand createGuildApplicationCommand(String applicationId, String guildId,
                                                            String name, String description, ApplicationCommand.Option[] options,
                                                            boolean default_permissions, int type) throws IOException {

        String subpath = String.format("/applications/%s/guilds/%s/commands", applicationId, guildId);

        ApplicationCommand cmd = new ApplicationCommand();
        cmd.name = name;
        cmd.description = description;
        cmd.options = options;
        cmd.default_permission = default_permissions;
        cmd.type = type;

        String rz = internalHttpRequest(POST, subpath, gson.toJson(cmd));
        return gson.fromJson(rz, ApplicationCommand.class);
    }

    public ApplicationCommand createGuildApplicationCommand(String applicationId, String guildId, ApplicationCommand cmd) throws IOException {
        String subpath = String.format("/applications/%s/guilds/%s/commands", applicationId, guildId);
        String rz = internalHttpRequest(POST, subpath, gson.toJson(cmd));
        return gson.fromJson(rz, ApplicationCommand.class);
    }

    public void deleteGuildApplicationCommand(String applicationId, String guildId, String commandId) throws IOException {
        String subpath = String.format("/applications/%s/guilds/%s/commands/%s", applicationId, guildId, commandId);
        internalHttpRequest(GET, subpath);
    }

    public ApplicationCommand[] bulkOverrideGuildApplicationCommands(String applicationId, String guildId, ApplicationCommand[] commands) throws IOException {
        String subpath = String.format("/applications/%s/guilds/%s/commands", applicationId, guildId);
        String rz = internalHttpRequest(PUT, subpath, gson.toJson(commands));
        return gson.fromJson(rz, ApplicationCommand[].class);
    }

    public void respondInteraction(String interactionId, String interactionToken, InteractionResponse response) throws IOException {
        String subpath = String.format("/interactions/%s/%s/callback", interactionId, interactionToken);
        String rz = internalHttpRequest(POST, subpath, gson.toJson(response));
    }


    // ====================================== //
    //                WEBHOOKS                //
    // ====================================== //

    public Webhook createWebhook(String channel, String name) throws IOException {
        String subpath = String.format("/channels/%s/webhooks", channel);

        JsonObject json = new JsonObject();
        json.addProperty("name", name);

        String rz = internalHttpRequest(POST, subpath, json.toString());
        return gson.fromJson(rz, Webhook.class);
    }

    public Webhook[] getChannelWebhooks(String channel) throws IOException {
        return gson.fromJson(intf(GET, "/channels/%s/webhooks", channel), Webhook[].class);
    }

    public Webhook[] getGuildWebhooks(String channel) throws IOException {
        return gson.fromJson(intf(GET, "/guilds/%s/webhooks", channel), Webhook[].class);
    }

    public Webhook getWebhook(String webhookId) throws IOException {
        return gson.fromJson(intf(GET, "/webhooks/%s", webhookId), Webhook.class);
    }

    public Webhook getWebhookWithToken(String webhookId, String token) throws IOException {
        String rz = internalHttpRequest(GET, String.format("/webhooks/%s/%s", webhookId, token), null, DEFAULT_RETRY_LIMIT, true, false);
        return gson.fromJson(rz, Webhook.class);
    }

    public Webhook modifyWebhook(String webhookId, String name, String target_channel) throws IOException {
        String subpath = String.format("/webhooks/%s", webhookId);

        JsonObject json = new JsonObject();
        json.addProperty("name", name);
        json.addProperty("channel_id", target_channel);

        String rz = internalHttpRequest(PATCH, subpath, json.toString());
        return gson.fromJson(rz, Webhook.class);
    }

    public Webhook modifyWebhookWithToken(String webhookId, String token, String name, String target_channel)
            throws IOException {
        String subpath = String.format("/webhooks/%s/%S", webhookId, token);

        JsonObject json = new JsonObject();
        json.addProperty("name", name);
        json.addProperty("channel_id", target_channel);

        String rz = internalHttpRequest(PATCH, subpath, json.toString(), DEFAULT_RETRY_LIMIT, true, false);
        return gson.fromJson(rz, Webhook.class);
    }

    public void deleteWebhook(String webhookId) throws IOException {
        internalHttpRequest(DELETE, String.format("/webhooks/%s", webhookId), null);
    }

    public void deleteWebhookWithToken(String webhookId, String token) throws IOException {
        internalHttpRequest(DELETE, String.format("/webhooks/%s/%s", webhookId, token), null, DEFAULT_RETRY_LIMIT, true, false);
    }

    public void executeWebhook(String webhookId, String token, WebhookData data)
            throws IOException {
        internalHttpRequest(POST, String.format("/webhooks/%s/%s", webhookId, token), gson.toJson(data), DEFAULT_RETRY_LIMIT, true, false);
    }

    public Message getWebhookMessage(String webhookId, String token, String messageId) throws IOException {
        String rz = internalHttpRequest(GET, String.format("/webhooks/%s/%s/messages/%s", webhookId, token, messageId), null, DEFAULT_RETRY_LIMIT, true, false);
        return gson.fromJson(rz, Message.class);
    }

    public void deleteWebhookMessage(String webhookId, String token, String messageId) throws IOException {
        internalHttpRequest(DELETE, String.format("/webhooks/%s/%s/messages/%s", webhookId, token, messageId), null, DEFAULT_RETRY_LIMIT, true, false);
    }
}
